;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; DESCRIPTION ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

; This game is an adaptation of the Russian Cards Problems: 3-3-2. 
; In this game, numbers are dealt in a matter of 3 for Alice, 3 for Bob and 2 for Trudy.
; (Trudy has 2 cards to make a better game for all the players)
; Recall that in the original Russians Cards Problems the objective is for Alice and Bob to exchange public information about their cards without Alice to be able to figure out anything. 
; This game is therefore a cooperation game between Alice and Bob. Though, also a zero-sum game with Trudy. 
; Trudy is the probable player to win this game, since she wins if the other players can't figure out one of her cards.
; The objective of the game is for:
;   Alice and Bob, share information about their own cards in order to find out Trudy's cards (the description is according to common knowledge about one of Trudy's cards, because the way the game is described, if Alice knows Bob's cards and Bob Alice's, then Trudy's cards will always be common knowledge, ence only necessary to define the rule for one card).
; This game is half of the normal size (15 rounds)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; ROLES ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(role random)
(role alice)
(role bob)
(role trudy)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; INITIAL STATE ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(init (round 0))
(init (turn bob))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; LEGAL MOVES ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; Random deals cards in the initial round
(<= (legal random (deal ?a1 ?a2 ?a3 ?b1 ?b2 ?b3 ?t1 ?t2))
    (alldistinct ?a1 ?a2 ?a3 ?b1 ?b2 ?b3 ?t1 ?t2)
    (true (round 0))
    )

; Random does nothing if it is not the first round
(<= (legal random noop) 
    (not (true (round 0)))
    )

; Trudy can do normal noop 
(legal trudy noop) 

; Bob and Alice change public announcements - triplet of numbers, where exactly one must be true
; They receive their percepts at turn 1 to 2, therefore they can't make this move at 1.
(<= (legal ?r (have ?x ?y ?z))
    (true (turn ?r)) (number ?y) (number ?z)
    (true (has ?r ?x)) (not (true (has ?r ?y))) (not (true (has ?r ?z)))
    (distinct ?y ?z)
    (not (true (round 0))) (not (true (round 1)))
    )
(<= (legal ?r (have ?x ?y ?z))
    (true (turn ?r)) (number ?x) (number ?z)
    (true (has ?r ?y)) (not (true (has ?r ?x))) (not (true (has ?r ?z)))
    (distinct ?x ?z)
    (not (true (round 0))) (not (true (round 1)))
    )
(<= (legal ?r (have ?x ?y ?z))
    (true (turn ?r)) (number ?x) (number ?y)
    (true (has ?r ?z)) (not (true (has ?r ?x))) (not (true (has ?r ?y)))
    (distinct ?x ?y)
    (not (true (round 0))) (not (true (round 1)))
    )

; Alice and Bob do noop if is either round 0 or is the other's turn to play
(<= (legal alice noop) 
    (or (true (round 0)) (true (round 1)) (true (turn bob)))
    )

(<= (legal bob noop) 
    (or (true (round 0)) (true (round 1)) (true (turn alice)))
    )

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; STATE UPDATE ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; Increase the round of the game
(<= (next (round ?n)) (true (round ?m)) (succ ?m ?n))

; For percepts
(<= (next (hasAlice ?a1 ?a2 ?a3)) 
    (does random (deal ?a1 ?a2 ?a3 ?b1 ?b2 ?b3 ?t1 ?t2))
    )
(<= (next (hasBob ?b1 ?b2 ?b3)) 
    (does random (deal ?a1 ?a2 ?a3 ?b1 ?b2 ?b3 ?t1 ?t2))
    )
(<= (next (hasTrudy ?t1 ?t2 )) 
    (does random (deal ?a1 ?a2 ?a3 ?b1 ?b2 ?b3 ?t1 ?t2))
    )

    ; Inertia
(<= (next (hasAlice ?a1 ?a2 ?a3)) 
    (true (hasAlice ?a1 ?a2 ?a3))
    )
(<= (next (hasBob ?b1 ?b2 ?b3)) 
    (true (hasBob ?b1 ?b2 ?b3))   
    )
(<= (next (hasTrudy ?t1 ?t2)) 
    (true (hasTrudy ?t1 ?t2))
    )

; For knowledge
(<= (next (has alice ?a1))
    (does random (deal ?a1 ?a2 ?a3 ?b1 ?b2 ?b3 ?t1 ?t2))
    )
(<= (next (has alice ?a2))
    (does random (deal ?a1 ?a2 ?a3 ?b1 ?b2 ?b3 ?t1 ?t2))
    )
(<= (next (has alice ?a3))
    (does random (deal ?a1 ?a2 ?a3 ?b1 ?b2 ?b3 ?t1 ?t2))
    )
(<= (next (has bob ?b1))
    (does random (deal ?a1 ?a2 ?a3 ?b1 ?b2 ?b3 ?t1 ?t2))
    )
(<= (next (has bob ?b2))
    (does random (deal ?a1 ?a2 ?a3 ?b1 ?b2 ?b3 ?t1 ?t2))
    )
(<= (next (has bob ?b3))
    (does random (deal ?a1 ?a2 ?a3 ?b1 ?b2 ?b3 ?t1 ?t2))
    )
(<= (next (has trudy ?t1))
    (does random (deal ?a1 ?a2 ?a3 ?b1 ?b2 ?b3 ?t1 ?t2))
    )
(<= (next (has trudy ?t2))
    (does random (deal ?a1 ?a2 ?a3 ?b1 ?b2 ?b3 ?t1 ?t2))
    )

    ; Inertia
(<= (next (has ?r ?n)) 
    (true (has ?r ?n))
    )

(<= (next (turn bob)) (true (turn alice)))
(<= (next (turn alice)) (true (turn bob)))
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; PERCEPTS ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; Alice sees her cards
(<= (sees alice (cards3 ?a1 ?a2 ?a3))
    (true (hasAlice ?a1 ?a2 ?a3)))

; Bob sees his cards
(<= (sees bob (cards3 ?b1 ?b2 ?b3))
     (true (hasBob ?b1 ?b2 ?b3)))

; Trudy sees her cards
(<= (sees trudy (card2 ?t1 ?t2))
    (true (hasTrudy ?t1 ?t2)))

; All the players are warned about the public announcement (except random)
(<= (sees ?o (haveOne ?r ?x ?y ?z))
    (role ?o) (does ?r (have ?x ?y ?z)) (distinct ?o random))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; TERMINALS ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; Game ends at round 16 
; or is common knowledge one of Trudy's cards (doesn't matter because the way the game is described, 
; when is common knowledge one of her cards, it is common knowledge the other)

(<= terminal (true (round 16)))
(<= terminal (knows (hasNumber trudy ?n)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; GOALS ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(goal random 100)

(<= (goal trudy 100) 
    (true (round 16))
    )

(<= (goal trudy 0) 
    (not (true (round 16)))
    )

(<= (goal alice 100) 
    (knows (hasNumber trudy ?n))
    )
(<= (goal alice 0) 
    (number ?n)
    (not (knows (hasNumber trudy ?n)))
    )

(<= (goal bob 100) 
    (knows (hasNumber trudy ?n))
    )
(<= (goal bob 0) 
    (number ?n)
    (not (knows (hasNumber trudy ?n)))
    )

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; AUXILIARS ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; Player has a number if it's true he has the number
(<= (hasNumber ?r ?n) (true (has ?r ?n)))


; all cards have to be different from each other 
; a1 != a2, a1 != a3, a1 != b1, a1 != b2, a1 != b3, a1 != t1, a1 != t2
; a2 != a3, a2 != b1, a2 != b2, a2 != b3, a2 != t1, a2 != t2 
; a3 != b1, a3 != b2, a3 != b3, a3 != t1, a3 != t2
; b1 != b2, b1 != b3, b1 != t1, b1 != t2
; b2 != b3, b2 != t1, b2 != t2
; b3 != t2, b3 != t2
; t1 != t2
(<= (alldistinct ?a1 ?a2 ?a3 ?b1 ?b2 ?b3 ?t1 ?t2) 
    (number ?a1) (number ?a2) (number ?a3) (number ?b1) (number ?b2) (number ?b3) (number ?t1) (number ?t2) 
    (distinct ?a1 ?a2) (distinct ?a1 ?a3) (distinct ?a1 ?b1) (distinct ?a1 ?b2) (distinct ?a1 ?b3) (distinct ?a1 ?t1) (distinct ?a1 ?t2)
    (distinct ?a2 ?a3) (distinct ?a2 ?b1) (distinct ?a2 ?b2) (distinct ?a2 ?b3) (distinct ?a2 ?t1) (distinct ?a2 ?t2)
    (distinct ?a3 ?b1) (distinct ?a3 ?b2) (distinct ?a3 ?b3) (distinct ?a3 ?t1) (distinct ?a3 ?t2)
    (distinct ?b1 ?b2) (distinct ?b1 ?b3) (distinct ?b1 ?t1) (distinct ?b1 ?t2)
    (distinct ?b2 ?b3) (distinct ?b2 ?t1) (distinct ?b2 ?t2)
    (distinct ?b3 ?t1) (distinct ?b3 ?t2)
    (distinct ?t1 ?t2))


(number 0)
(number 1)
(number 2)
(number 3)
(number 4)
(number 5)
(number 6)
(number 7)

(succ 0 1)
(succ 1 2)
(succ 2 3)
(succ 3 4)
(succ 4 5)
(succ 5 6)
(succ 6 7)
(succ 7 8)
(succ 8 9)
(succ 9 10)
(succ 10 11)
(succ 11 12)
(succ 12 13)
(succ 13 14)
(succ 14 15)
(succ 15 16)
(succ 16 17)
(succ 17 18)
(succ 18 19)
(succ 19 20)
(succ 20 21)
(succ 21 22)
(succ 22 23)
(succ 23 24)
(succ 24 25)
(succ 25 26)
(succ 26 27)
(succ 27 28)
(succ 28 29)
(succ 29 30)
(succ 30 31)
(succ 31 32)