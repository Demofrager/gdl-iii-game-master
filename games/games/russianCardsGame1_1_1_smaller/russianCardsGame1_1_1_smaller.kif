;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; DESCRIPTION ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;This game is an adaptation of the Russian Cards Problems: 1-1-1. But there are 4 different numbers.
;In this game, numbers are dealt in a matter of 1 for Alice, 1 for Bob and 1 for Trudy. 
;Bob and Alice then exchange public announcements of their cards in order to figure out Trudy's card. 
;It is a cooperation game between Alice and Bob but also a zero-sum game with Trudy. 
;Trudy, can only move in the round 3, if and only if she knows that another player has a specific card she can swap that with one of hers'.  
;The objective of the game is:
;   Trudy - disturb the other players so that they can't figure out her cards in the total rounds of the game. 
;   Alice and Bob - share information about their cards in order to find out Trudy's card (the description is according to common knowledge about Trudy's cards, because according to the way the game is described, if Alice knows Bob's cards and Bob Alice's, then Trudy's cards will always be common knowledge.
; The size of this game is 6 rounds.

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; ROLES ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(role random)
(role alice)
(role bob)
(role trudy)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; INITIAL STATE ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(init (round 0))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; LEGAL MOVES ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; Random deals cards in the initial round
(<= (legal random (deal ?a1 ?b1 ?t1))
    (true (round 0))
    (number ?a1) (number ?b1) (number ?t1)
    (distinct ?a1 ?b1) (distinct ?a1 ?t1) (distinct ?b1 ?t1)
    )

; Random does nothing if it is not the first round
(<= (legal random noop)
    (not (true (round 0)))
    )

; Trudy can do normal noop 
(<= (legal trudy noop) 
    (not (true (turn trudy)))
    )

; Trudy can do a different noop if is her turn to play and she does not know any of the others cards *(INB4: knowledge debug)
; Not working fully
(<= (legal trudy noop1) 
    (true (turn trudy))
    )    

; Trudy can switch a card with alice player if she knows that she has that number
(<= (legal trudy (switchalice ?t1 ?a1)) 
    (true (turn trudy)) 
    (true (has trudy ?t1))
    (true (has alice ?a1))
    )

; Trudy can switch a card with bob player if she knows that he has that number
(<= (legal trudy (switchbob ?t1 ?b1)) 
    (true (turn trudy)) 
    (true (has trudy ?t1))
    (true (has bob ?b1))
    )


; Bob and Alice make public announcements about their card, in a triplet, where one card is for sure theirs, but the others can be any subset of the Cartesian product of the remaining numbers
(<= (legal ?r (have ?n1 ?n2 ?n3))
    (true (turn ?r)) (number ?n2) (number ?n3) (role ?r) (distinct ?r trudy)
    (true (has ?r ?n1)) (not (true (has ?r ?n2))) (not (true (has ?r ?n3)))
    )
(<= (legal ?r (have ?n1 ?n2 ?n3))
    (true (turn ?r)) (number ?n1) (number ?n3) (role ?r) (distinct ?r trudy)
    (not (true (has ?r ?n1)))  (true (has ?r ?n2)) (not (true (has ?r ?n3)))
    )

(<= (legal ?r (have ?n1 ?n2 ?n3))
    (true (turn ?r)) (number ?n1) (number ?n2) (role ?r) (distinct ?r trudy)
    (not (true (has ?r ?n1))) (not (true (has ?r ?n2)))  (true (has ?r ?n3))
    )

(<= (legal alice noop)
    (not (true (turn alice))) 
    )

(<= (legal bob noop)
    (not (true (turn bob))) 
    )

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; STATE UPDATE ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; Increase the round of the game
(<= (next (round ?n)) (true (round ?m)) (succ ?m ?n))

; Players have the numbers as soon as random deals them
(<= (next (has alice ?a1))
    (does random (deal ?a1 ?b1 ?t1))
    )
(<= (next (has bob ?b1))
    (does random (deal ?a1 ?b1 ?t1))
    )
(<= (next (has trudy ?t1))
    (does random (deal ?a1 ?b1 ?t1))
    )

; Players keep their numbers if is not trudy's turn or she does noop1 at her turn
(<= (next (has ?r ?n))
    (true (has ?r ?n))
    (not (true (turn trudy)))
    )
(<= (next (has ?r ?n))
    (true (has ?r ?n))
    (true (turn trudy))
    (does trudy noop1)
    )

; At Trudy's turn can privatly switch a card 
(<= (next (has alice ?a1))
    (true (has alice ?a1))
    (true (turn trudy))
    (does trudy (switchbob ?t1 ?b1))
    )
(<= (next (has bob ?b1))
    (true (has bob ?b1))
    (true (turn trudy))
    (does trudy (switchalice ?t1 ?a1))
    )

; Players only keep the switched card that Trudy gave them
(<= (next (has alice ?t1))
    (true (turn trudy))
    (does trudy (switchalice ?t1 ?a1)) 
    (true (has alice ?a1))
    )
(<= (next (has bob ?t1))
    (true (turn trudy))
    (does trudy (switchbob ?t1 ?b1)) 
    (true (has bob ?b1))
    )
(<= (next (has trudy ?n1))
    (true (turn trudy))
    (does trudy (switchalice ?t1 ?n1)) 
    (true (has alice ?n1))
    )
(<= (next (has trudy ?n1))
    (true (turn trudy))
    (does trudy (switchbob ?t1 ?n1)) 
    (true (has bob ?n1))
    )

; Turn iteration
(<= (next (turn alice)) 
    (or 
        (true (turn trudy)) (true (round 0))
        )
    )
(<= (next (turn bob))
    (true (turn alice))
    )
(<= (next (turn trudy))
    (true (turn bob))
    )

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; PERCEPTS ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; NOTE - at beggining of turn 4 if Trudy switches the card with someone, that someone is going to see 2 percepts: one that cooresponds to the card that he currently owns and the other, that represents the swap action of trudy. Meaning that "You currently own this card, but trudy switched it. So in the next turn the only percept you'll recieve is the new card".
; Alice sees her card
(<= (sees alice (cardA ?a1))
    (or (true (has alice ?a1)) (does random (deal ?a1 ?b1 ?t1)) )
    )

; Bob sees his card
(<= (sees bob (cardB ?b1))
     (or (true (has bob ?b1)) (does random (deal ?a1 ?b1 ?t1)) )
    )
; Trudy sees her card
(<= (sees trudy (cardT ?t1))
    (or (true (has trudy ?t1)) (does random (deal ?a1 ?b1 ?t1)) )
    )

(<= (sees trudy (deal ?a1 ?b1 ?t1))
    (does random (deal ?a1 ?b1 ?t1)) 
    )

; All the players are warned about the public announcement (except random)
(<= (sees ?o (haveOne ?r ?x ?y ?z))
    (role ?o) (does ?r (have ?x ?y ?z)) (distinct ?o random))

; Whomever trudy switchs her card with, gets warned
(<= (sees alice (swap ?trudyC ?rC))
    (does trudy (switchalice ?trudyC ?rC))
    )
(<= (sees bob (swap ?trudyC ?rC))
    (does trudy (switchbob ?trudyC ?rC))
    )

; Of course trudy as well
(<= (sees trudy (swap ?trudyC ?rC))
    (does trudy (switchalice ?trudyC ?rC))
    )
(<= (sees trudy (swap ?trudyC ?rC))
    (does trudy (switchbob ?trudyC ?rC))
    )

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; TERMINALS ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; Game ends at round 6 
; or is public knowledge one of trudy's card (doesn't matter because the way the game is described, 
; when is public knowledge one of her cards, it is public knowledge the other)

(<= terminal (true (round 6)))
(<= terminal 
    (commonKnowledge)
    )

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; GOALS ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; The objective of the game is for trudy to disrupt Alice and Bob's reasoning.
; And for alice and bob to know Trudy's card.

(goal random 100)

(<= (goal trudy 100) 
    (true (round 6))
    )
(<= (goal trudy 0) 
    (not (true (round 6)))
    )

(<= (goal alice 100) 
    (commonKnowledge) 
    )
(<= (goal alice 0)
    (not (commonKnowledge))
    )

(<= (goal bob 100) 
    (commonKnowledge) 
    )
(<= (goal bob 0) 
    (not (commonKnowledge))
    )

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; AUXILIARS ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; Please don't judge me. I don't know what to do more, so that this description works.......
(<= (hasTrudy ?t1) 
    (true (has trudy ?t1))
    )

(<= (commonKnowledge) 
    (knows (hasTrudy ?t1))
    )

(number 0)
(number 1)
(number 2)

(succ 0 1)
(succ 1 2)
(succ 2 3)
(succ 3 4)
(succ 4 5)
(succ 5 6)
