package sena.utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.List;

import org.ggp.base.util.game.Game;
import org.ggp.base.util.game.GameRepository;
import org.ggp.base.util.gdl.factory.exceptions.GdlFormatException;
import org.ggp.base.util.gdl.grammar.Gdl;
import org.ggp.base.util.gdl.model.SentenceDomainModel;
import org.ggp.base.util.gdl.model.SentenceDomainModelFactory;
import org.ggp.base.util.gdl.transforms.CondensationIsolator;
import org.ggp.base.util.gdl.transforms.DeORer;
import org.ggp.base.util.gdl.transforms.GdlCleaner;
import org.ggp.base.util.gdl.transforms.Relationizer;
import org.ggp.base.util.gdl.transforms.VariableConstrainer;
import org.ggp.base.util.statemachine.Role;
import org.ggp.base.util.statemachine.exceptions.GoalDefinitionException;
import org.ggp.base.util.statemachine.exceptions.MoveDefinitionException;
import org.ggp.base.util.statemachine.exceptions.TransitionDefinitionException;
import org.ggp.base.util.symbol.factory.exceptions.SymbolFormatException;

import sena.base.util.statemachine.exceptions.PerceptDefinitionException;
import sena.base.util.statemachine.implementation.prover.III_ProverStateMachine;

public class KnowledgeTutorialOtherWay {

	public static void main(String[] args) throws SecurityException, IOException, MoveDefinitionException,
			TransitionDefinitionException, PerceptDefinitionException, GdlFormatException, SymbolFormatException,
			GoalDefinitionException, InterruptedException {



	BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

	// Recieves the game from here. Needs change
	Game game = GameRepository.getDefaultRepository().getGame("numberGuessingEpistemic");
	// Game game =
	// GameRepository.getDefaultRepository().getGame("muddyChildren");
	List<Role> roles = Role.computeRoles(game.getRules());

	III_ProverStateMachine stateMachine = new III_ProverStateMachine();

	List<Gdl> gameRules = game.getRules();

	gameRules=GdlCleaner.run(gameRules);
	gameRules=DeORer.run(gameRules);
	gameRules=VariableConstrainer.replaceFunctionValuedVariables(gameRules);
	gameRules=Relationizer.run(gameRules);

	gameRules=CondensationIsolator.run(gameRules);

	// System.out.println(gameRules);

	// We want to start with a rule graph and follow the rule graph.
	// Start by finding general information about the game
	SentenceDomainModel model = SentenceDomainModelFactory.createWithCartesianDomains(gameRules);
	System.out.println(model.getSentenceForms());
}}
