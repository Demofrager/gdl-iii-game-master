package sena.utils;

import java.util.concurrent.TimeUnit;

import com.google.common.base.Stopwatch;

public class TryStopWatch {
	private static Stopwatch watch;
	private static long total;

	public static void main(String[] args){
		watch = new Stopwatch();
		total = 0L;


		watch.start();
		System.out.println("Clock started");
		for (int i = 0; i < 11; i ++){
			System.out.println(watch.stop());
			total += watch.elapsed(TimeUnit.MICROSECONDS);
			System.out.println(watch.elapsed(TimeUnit.MICROSECONDS));
			watch.start();
		}

		System.out.println(watch.stop());

		System.out.println("Printing statistics: ");
		System.out.println(total / 11 + " μs/round");
	}
}
