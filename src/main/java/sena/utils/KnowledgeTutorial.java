package sena.utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.List;

import org.ggp.base.util.game.Game;
import org.ggp.base.util.game.GameRepository;
import org.ggp.base.util.gdl.factory.exceptions.GdlFormatException;
import org.ggp.base.util.gdl.grammar.Gdl;
import org.ggp.base.util.statemachine.MachineState;
import org.ggp.base.util.statemachine.Role;
import org.ggp.base.util.statemachine.exceptions.GoalDefinitionException;
import org.ggp.base.util.statemachine.exceptions.MoveDefinitionException;
import org.ggp.base.util.statemachine.exceptions.TransitionDefinitionException;
import org.ggp.base.util.symbol.factory.exceptions.SymbolFormatException;

import sena.base.util.statemachine.exceptions.PerceptDefinitionException;
import sena.base.util.statemachine.implementation.prover.III_ProverStateMachine;

public class KnowledgeTutorial {

	private static final int BAG_SIZE = 4;

	public static void main(String[] args) throws SecurityException, IOException, MoveDefinitionException,
			TransitionDefinitionException, PerceptDefinitionException, GdlFormatException, SymbolFormatException,
			GoalDefinitionException, InterruptedException {

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));


		Game game = GameRepository.getDefaultRepository().getGame("numberGuessingEpistemic");
		//Game game =  GameRepository.getDefaultRepository().getGame("muddyChildren");
		//Game game =  GameRepository.getDefaultRepository().getGame("testV3");
		List<Role> roles = Role.computeRoles(game.getRules());

		III_ProverStateMachine stateMachine = new III_ProverStateMachine();
		List<Gdl> gameRules = game.getRules();


		stateMachine.initialize(gameRules, 0, 0);

		MachineState currentState = stateMachine.getInitialState();


		System.out.println(currentState);

		// Iterator<GdlRule> itGdl = flatDescription.iterator();
//		Iterator<Gdl> itGdl = gameRules.iterator();
//		Set<GdlSentence> knowsL = new HashSet<GdlSentence>();
//		Set<GdlRule> rulesL = new HashSet<GdlRule>();
//
//		while (itGdl.hasNext()) {
//			Gdl next = itGdl.next();
//			// System.out.println(next);
//			if (next instanceof GdlRule) { // keyword knows only appears in the
//											// body of the rules
//				List<GdlLiteral> body = ((GdlRule) next).getBody();
//				Iterator<GdlLiteral> itBody = body.iterator();
//				while (itBody.hasNext()) {
//					GdlLiteral bodyN = itBody.next();
//					if (bodyN instanceof GdlSentence) {
//						if (((GdlSentence) bodyN).getName().equals(GdlPool.KNOWS)) {
//							GdlSentence knows = ((GdlSentence) bodyN);
//
//							knowsL.add(knows);
//							rulesL.add((GdlRule) next);
//						}
//					}
//				}
//			}
//		}
//
//		//System.out.println(knowsL);
//		//System.out.println(rulesL);
//		//System.out.println();
//
//		Iterator<GdlSentence> knowsIt = knowsL.iterator();
//		GdlSentence knows_ = knowsIt.next();
//
//		Iterator<GdlRule> rulesIt = rulesL.iterator();
//		GdlRule rule = rulesIt.next();

		//System.out.println(knows_.get(0).equals(knows_.get(1).toSentence().get(0)));
		//System.out.println();

		currentState = stateMachine.getRandomNextState(currentState);
		System.out.println(currentState);
	//	System.out.println(stateMachine.getLegalJointMoves(currentState));
		//stateMachine.getKnowledgeTerms(currentState, gameRules);



		System.exit(0);
	}

}
