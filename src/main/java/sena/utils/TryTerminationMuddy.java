package sena.utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.ggp.base.util.game.Game;
import org.ggp.base.util.game.GameRepository;
import org.ggp.base.util.gdl.factory.GdlFactory;
import org.ggp.base.util.gdl.factory.exceptions.GdlFormatException;
import org.ggp.base.util.gdl.grammar.Gdl;
import org.ggp.base.util.gdl.grammar.GdlPool;
import org.ggp.base.util.gdl.grammar.GdlSentence;
import org.ggp.base.util.statemachine.MachineState;
import org.ggp.base.util.statemachine.Move;
import org.ggp.base.util.statemachine.Role;
import org.ggp.base.util.statemachine.exceptions.MoveDefinitionException;
import org.ggp.base.util.statemachine.exceptions.TransitionDefinitionException;
import org.ggp.base.util.symbol.factory.exceptions.SymbolFormatException;

import sena.base.knowledge.bag.exception.ModelNotFoundException;
import sena.base.util.statemachine.exceptions.PerceptDefinitionException;
import sena.base.util.statemachine.implementation.prover.III_ProverStateMachine;

public class TryTerminationMuddy {

	public static void main(String[] args)
			throws SecurityException, IOException, MoveDefinitionException, TransitionDefinitionException,
			PerceptDefinitionException, GdlFormatException, SymbolFormatException, ModelNotFoundException {

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		// Game game =
		// GameRepository.getDefaultRepository().getGame("numberGuessingEpistemic");
		Game game = GameRepository.getDefaultRepository().getGame("muddyChildren");
		// Game game = GameRepository.getDefaultRepository().getGame("testV3");
		List<Role> roles = Role.computeRoles(game.getRules());

		III_ProverStateMachine stateMachine = new III_ProverStateMachine();
		List<Gdl> gameRules = game.getRules();

		stateMachine.initialize(gameRules, 0, 0);

		MachineState currentState = stateMachine.getInitialState();

		Move m = new Move(GdlFactory.createTerm("( muddy 0 1 0 )"));
		List<Move> jointMoves = stateMachine.getRandomJointMove(currentState, new Role(GdlPool.RANDOM), m);
		System.out.println(jointMoves);

		Set<GdlSentence> newCache = new HashSet<>(currentState.getContents());
		newCache.add((GdlSentence) GdlFactory.create("( knows bob ( isMuddy bob ) )"));

		MachineState newState = new MachineState(newCache);

		System.out.println(stateMachine.getLegalJointMoves(newState));
		/*int i = 0;
		while (!stateMachine.isTerminal(currentState)) {
			System.out.println(currentState);
			List<Move> jointMoves = stateMachine.getRandomJointMove(currentState);
			System.out.println(jointMoves);
			System.out.println(stateMachine.getPercepts(currentState, jointMoves));
			currentState = stateMachine.getNextState(currentState, jointMoves);

			if (i > 1) {
				newCache.add(GdlFactory.createTerm("(knows bob (isMuddy bob))").toSentence());
				stateMachine.hackCache(newCache);
			}
			i++;
		}*/
		System.exit(0);
	}
}
