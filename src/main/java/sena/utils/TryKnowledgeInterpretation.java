package sena.utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import org.ggp.base.util.game.Game;
import org.ggp.base.util.game.GameRepository;
import org.ggp.base.util.gdl.factory.exceptions.GdlFormatException;
import org.ggp.base.util.gdl.grammar.Gdl;
import org.ggp.base.util.gdl.grammar.GdlSentence;
import org.ggp.base.util.statemachine.MachineState;
import org.ggp.base.util.statemachine.Move;
import org.ggp.base.util.statemachine.Role;
import org.ggp.base.util.statemachine.exceptions.MoveDefinitionException;
import org.ggp.base.util.statemachine.exceptions.TransitionDefinitionException;
import org.ggp.base.util.symbol.factory.exceptions.SymbolFormatException;

import sena.base.knowledge.bag.exception.ModelNotFoundException;
import sena.base.util.statemachine.exceptions.PerceptDefinitionException;
import sena.base.util.statemachine.implementation.prover.III_ProverStateMachine;

public class TryKnowledgeInterpretation {

	public static void main(String[] args)
			throws SecurityException, IOException, MoveDefinitionException, TransitionDefinitionException,
			PerceptDefinitionException, GdlFormatException, SymbolFormatException, ModelNotFoundException {

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		//Game game = GameRepository.getDefaultRepository().getGame("numberGuessingEpistemic32");
		//Game game = GameRepository.getDefaultRepository().getGame("muddyChildren3");
		 Game game = GameRepository.getDefaultRepository().getGame("testV3");

		List<Role> roles = Role.computeRoles(game.getRules());

		III_ProverStateMachine stateMachine = new III_ProverStateMachine();
		List<Gdl> gameRules = game.getRules();

		stateMachine.initialize(gameRules, 0, 0);

		MachineState currentState = stateMachine.getInitialState();
		HashSet<GdlSentence> context = new HashSet<>();
		Set<GdlSentence> newCache = new HashSet<>();

		List<Move> jointMoves = new LinkedList<>();
		System.out.println(currentState);
		jointMoves = stateMachine.getRandomJointMove(currentState);
		System.out.println(jointMoves);
		System.out.println(stateMachine.getPercepts(currentState, jointMoves));
		currentState = stateMachine.getNextState(currentState, jointMoves);

		stateMachine.setupTargetKnowledge(currentState, gameRules);

		System.exit(0);

	}

}
