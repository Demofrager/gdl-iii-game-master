package sena.base.util.statemachine.implementation.prover;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import org.ggp.base.util.gdl.grammar.Gdl;
import org.ggp.base.util.gdl.grammar.GdlConstant;
import org.ggp.base.util.gdl.grammar.GdlRelation;
import org.ggp.base.util.gdl.grammar.GdlSentence;
import org.ggp.base.util.logging.GamerLogger;
import org.ggp.base.util.prover.Prover;
import org.ggp.base.util.prover.aima.AimaProver;
import org.ggp.base.util.statemachine.MachineState;
import org.ggp.base.util.statemachine.Move;
import org.ggp.base.util.statemachine.Role;
import org.ggp.base.util.statemachine.exceptions.GoalDefinitionException;
import org.ggp.base.util.statemachine.exceptions.MoveDefinitionException;
import org.ggp.base.util.statemachine.exceptions.TransitionDefinitionException;

import com.google.common.collect.ImmutableList;

import sena.base.util.statemachine.II_StateMachine;
import sena.base.util.statemachine.Percept;
import sena.base.util.statemachine.exceptions.PerceptDefinitionException;
import sena.base.util.statemachine.implementation.prover.query.II_ProverQueryBuilder;
import sena.base.util.statemachine.implementation.prover.result.II_ProverResultParser;

public class II_ProverStateMachine extends II_StateMachine {
	private MachineState initialState;
	private Prover prover;
	private ImmutableList<Role> roles;

	/**
	 * Initialize must be called before using the StateMachine
	 */
	public II_ProverStateMachine() {

	}

	@Override
	public void initialize(List<Gdl> description) {

		prover = new AimaProver(description);
		roles = ImmutableList.copyOf(Role.computeRoles(description));
		initialState = computeInitialState();
	}

	private MachineState computeInitialState() {

		Set<GdlSentence> results = prover.askAll(II_ProverQueryBuilder.getInitQuery(), new HashSet<GdlSentence>());
		return new II_ProverResultParser().toState(results);
	}

	@Override
	public int getGoal(MachineState state, Role role) throws GoalDefinitionException {
		Set<GdlSentence> results = prover.askAll(II_ProverQueryBuilder.getGoalQuery(role),
				II_ProverQueryBuilder.getContext(state));

		if (results.size() != 1) {
			GamerLogger.logError("StateMachine",
					"Got goal results of size: " + results.size() + " when expecting size one.");
			throw new GoalDefinitionException(state, role);
		}

		try {
			GdlRelation relation = (GdlRelation) results.iterator().next();
			GdlConstant constant = (GdlConstant) relation.get(1);

			return Integer.parseInt(constant.toString());
		} catch (Exception e) {
			throw new GoalDefinitionException(state, role);
		}
	}

	@Override
	public MachineState getInitialState() {
		return initialState;
	}

	@Override
	public List<Move> getLegalMoves(MachineState state, Role role) throws MoveDefinitionException {

		Set<GdlSentence> results = prover.askAll(II_ProverQueryBuilder.getLegalQuery(role),
				II_ProverQueryBuilder.getContext(state));
		if (results.isEmpty()) {
			throw new MoveDefinitionException(state, role);
		}

		return new II_ProverResultParser().toMoves(results);
	}

	private List<Percept> getPercepts(MachineState state, Role role, List<Move> moves) throws PerceptDefinitionException {
		Set<GdlSentence> results = prover.askAll(II_ProverQueryBuilder.getSeesQuery(role),
				II_ProverQueryBuilder.getContext(state, getRoles(), moves));

		if (results.isEmpty()) {
			return new LinkedList<Percept>();
		}

		return new II_ProverResultParser().toPercepts(role, results);
	}

	@Override
	public List<List<Percept>> getPercepts(MachineState state, List<Move> moves) throws PerceptDefinitionException {

		List<List<Percept>> percepts = new ArrayList<List<Percept>>();
		List<Percept> rolePercepts = new ArrayList<Percept>();
		for (Role role : roles) {
			rolePercepts = getPercepts(state, role, moves);
			percepts.add(rolePercepts);
		}
		return percepts;
	}

	@Override
	public MachineState getNextState(MachineState state, List<Move> moves) throws TransitionDefinitionException {
		Set<GdlSentence> results = prover.askAll(II_ProverQueryBuilder.getNextQuery(),
				II_ProverQueryBuilder.getContext(state, getRoles(), moves));

		for (GdlSentence sentence : results) {
			if (!sentence.isGround()) {
				throw new TransitionDefinitionException(state, moves);
			}
		}

		return new II_ProverResultParser().toState(results);

	}

	@Override
	public List<Role> getRoles() {
		return roles;
	}

	@Override
	public boolean isTerminal(MachineState state) {

		return prover.prove(II_ProverQueryBuilder.getTerminalQuery(), II_ProverQueryBuilder.getContext(state));
	}
}