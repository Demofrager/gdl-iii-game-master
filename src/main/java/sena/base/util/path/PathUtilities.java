package sena.base.util.path;

import java.util.ArrayList;
import java.util.List;

import javax.naming.SizeLimitExceededException;

import org.ggp.base.util.statemachine.Move;

import sena.base.util.statemachine.Percept;

/**
 * This class is to help on the sanitation of a path
 *
 * e.g. [[a,noop], [b,noop]]
 *
 * @author demofrager
 *
 */
public class PathUtilities {

	/**
	 * Splits a full path of a state and returns the player correspondent moves
	 *
	 * e.g. [a,b] = pathSplitter([[a,noop], [b,noop]], 0)
	 *
	 * @param fullPath
	 * @param roleID
	 * @return
	 */
	public static List<Move> movePathSplitter(List<List<Move>> fullPath, int roleID) {

		List<Move> toReturn = new ArrayList<Move>(fullPath.size());
		for (List<Move> jointMove : fullPath) {
			toReturn.add(jointMove.get(roleID));
		}
		return toReturn;
	}

	/**
	 * Splits a full path of a state and returns the player correspondent moves
	 * until a specified level
	 * e.g. [a] = pathSplitter([[a,noop], [b,noop]], 0, 0)
	 *		[a, b] = pathSplitter([[a,noop], [b,noop]], 0, 1)
	 * @param fullPath
	 * @param roleID
	 * @return
	 * @throws SizeLimitExceededException
	 */
	public static List<Move> movePathSplitter(List<List<Move>> fullPath, int roleID, int level) throws SizeLimitExceededException {

		if(level > fullPath.size()) throw new SizeLimitExceededException();
		List<Move> toReturn = new ArrayList<Move>(level);

		for(int i = 0; i <= level ; i++){
			toReturn.add(fullPath.get(i).get(roleID));
		}

		return toReturn;
	}

	public static List<List<Move>> movesPathBuilder(List<List<Move>> fullPath, List<Move> toAdd) {
		List<List<Move>> newPath = new ArrayList<List<Move>>(fullPath);
		newPath.add(toAdd);
		return newPath;
	}

	public static List<List<List<Percept>>> perceptsPathBuilder(List<List<List<Percept>>> fullPath,
			List<List<Percept>> toAdd) {
		List<List<List<Percept>>> newPath = new ArrayList<List<List<Percept>>>(fullPath);
		newPath.add(toAdd);
		return newPath;
	}

	/**
	 * Compares two paths
	 *
	 * @param fullPath1
	 * @param fullPath2
	 * @return
	 */
	public static boolean equals(List<List<Move>> fullPath1, List<List<Move>> fullPath2) {
		return fullPath1.equals(fullPath2);
	}

	/**Does the same thing as movePathSplitter but with a
	 * percept list
	 *
	 * @param actualPerceptHistory
	 * @param roleID
	 * @return
	 */
	public static List<List<Percept>> perceptsPathSplitter(List<List<List<Percept>>> actualPerceptHistory, int roleID) {
		List<List<Percept>> toReturn = new ArrayList<List<Percept>>(actualPerceptHistory.size());
		for (List<List<Percept>> jointPercept : actualPerceptHistory) {
			toReturn.add(jointPercept.get(roleID));
		}
		return toReturn;
	}

	/**Does the same thing as movePathSplitter but with a
	 * percept list
	 *
	 * @param actualPerceptHistory
	 * @param roleID
	 * @param level
	 * @return
	 * @throws SizeLimitExceededException
	 */
	public static List<List<Percept>> perceptsPathSplitter(List<List<List<Percept>>> actualPerceptHistory, int roleID, int level) throws SizeLimitExceededException {

		if(level > actualPerceptHistory.size()) throw new SizeLimitExceededException();

		List<List<Percept>> toReturn = new ArrayList<List<Percept>>(actualPerceptHistory.size());

		for(int i = 0; i <= level; i++){
			toReturn.add(actualPerceptHistory.get(i).get(roleID));
		}

		return toReturn;
	}
}
