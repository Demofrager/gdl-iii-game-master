package sena.base.server.request;

import java.util.List;

import org.ggp.base.util.gdl.grammar.Gdl;
import org.ggp.base.util.gdl.scrambler.GdlScrambler;
import org.ggp.base.util.statemachine.Move;
import org.ggp.base.util.statemachine.Role;

import sena.base.util.statemachine.Percept;

public final class II_RequestBuilder {
	private II_RequestBuilder() {
	}

	// Need to change this
	public static String getPlayRequest(String matchId, int turn, Move move, List<Percept> percepts,
			GdlScrambler scrambler) {

		// No previous move == first round; So no percepts as well
		if (move == null) {
			return "( PLAY " + matchId + " " + turn + " NIL " + "NIL )";
		} else {
			StringBuilder sb = new StringBuilder();

			sb.append("( PLAY " + matchId + " " + turn + " ");
			sb.append(scrambler.scramble(move.getContents()) + " ");

			// No percepts for the player
			if (percepts.isEmpty())
				sb.append("( ) )");
			else {
				sb.append("( ");
				for (Percept percept : percepts) {

					sb.append(scrambler.scramble(percept.getContents()) + " ");
				}
				sb.append(" )");

			}
			sb.append(" )");
			return sb.toString();
		}
	}

	public static String getStartRequest(String matchId, Role role, List<Gdl> description, int startClock,
			int playClock, GdlScrambler scrambler) {
		StringBuilder sb = new StringBuilder();

		sb.append("( START " + matchId + " " + scrambler.scramble(role.getName()) + " (");
		for (Gdl gdl : description) {
			sb.append(scrambler.scramble(gdl) + " ");
		}
		sb.append(") " + startClock + " " + playClock + ")");

		return sb.toString();
	}

	public static String getPreviewRequest(List<Gdl> description, int previewClock, GdlScrambler scrambler) {
		StringBuilder sb = new StringBuilder();

		sb.append("( PREVIEW (");
		for (Gdl gdl : description) {
			sb.append(scrambler.scramble(gdl) + " ");
		}
		sb.append(") " + previewClock + " )");

		return sb.toString();
	}

	public static String getStopRequest(String matchId, int turn, Move move, List<Percept> percepts,
			GdlScrambler scrambler) {

		// No previous move == first round; So no percepts as well
		if (move == null) {
			return "( STOP " + matchId + " " + turn + " NIL " + "NIL )";
		} else {
			StringBuilder sb = new StringBuilder();

			sb.append("( STOP " + matchId + " " + turn + " ");
			sb.append(scrambler.scramble(move.getContents()) + " ");

			// No percepts for the player
			if (percepts.isEmpty())
				sb.append("( ) )");
			else {
				sb.append("( ");
				for (Percept percept : percepts) {

					sb.append(scrambler.scramble(percept.getContents()) + "  ");
				}
				sb.append(") )");
			}

			return sb.toString();
		}
	}

	public static String getAbortRequest(String matchId) {
		return "( ABORT " + matchId + " )";
	}

	public static String getInfoRequest() {
		return "( INFO )";
	}
}
