package sena.base.server;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.ggp.base.server.event.ServerAbortedMatchEvent;
import org.ggp.base.server.event.ServerCompletedMatchEvent;
import org.ggp.base.server.event.ServerConnectionErrorEvent;
import org.ggp.base.server.event.ServerIllegalMoveEvent;
import org.ggp.base.server.event.ServerNewGameStateEvent;
import org.ggp.base.server.event.ServerNewMatchEvent;
import org.ggp.base.server.event.ServerNewMovesEvent;
import org.ggp.base.server.event.ServerTimeEvent;
import org.ggp.base.server.event.ServerTimeoutEvent;
import org.ggp.base.util.gdl.grammar.GdlPool;
import org.ggp.base.util.observer.Event;
import org.ggp.base.util.observer.Observer;
import org.ggp.base.util.observer.Subject;
import org.ggp.base.util.statemachine.MachineState;
import org.ggp.base.util.statemachine.Move;
import org.ggp.base.util.statemachine.Role;
import org.ggp.base.util.statemachine.exceptions.GoalDefinitionException;
import org.ggp.base.util.statemachine.exceptions.MoveDefinitionException;

import sena.base.server.event.II_ServerMatchUpdatedEvent;
import sena.base.server.threads.II_AbortRequestThread;
import sena.base.server.threads.II_PlayRequestThread;
import sena.base.server.threads.II_PreviewRequestThread;
import sena.base.server.threads.II_RandomPlayRequestThread;
import sena.base.server.threads.II_StartRequestThread;
import sena.base.server.threads.II_StopRequestThread;
import sena.base.util.match.II_Match;
import sena.base.util.match.II_MatchPublisher;
import sena.base.util.statemachine.II_StateMachine;
import sena.base.util.statemachine.Percept;
import sena.base.util.statemachine.implementation.prover.II_ProverStateMachine;

public final class II_GameServer extends Thread implements Subject {
	private final II_Match match;
	private final II_StateMachine stateMachine;
	private MachineState currentState;

	private final List<String> hosts;
	private final List<Integer> ports;
	private final boolean[] playerGetsUnlimitedTime;
	private final boolean[] playerPlaysRandomly;
	private final List<Observer> observers;
	private List<Move> previousMoves;
	private List<List<Percept>> percepts;

	private Map<Role, String> mostRecentErrors;

	private String saveToFilename;
	private String spectatorServerURL;
	private String spectatorServerKey;
	private boolean forceUsingEntireClock;
	/**
	 * This class is the game server for games in GDL-II.
	 *
	 * @param match - GDL-II match
	 * @param hosts - List of hosts (addresses of the players) that are going
	 * 	to play the game.
	 * @param ports - The ports of those hosts (players)
	 */
	public II_GameServer(II_Match match, List<String> hosts, List<Integer> ports) {

		this.match = match;

		this.hosts = hosts;
		this.ports = ports;

		playerGetsUnlimitedTime = new boolean[hosts.size()];
		playerPlaysRandomly = new boolean[hosts.size()];
		List<Role> roles = Role.computeRoles(match.getGame().getRules());


		// If Description has (role random)
		for (int r = 0; r < roles.size(); r++) {
			if (roles.get(r).getName() == GdlPool.RANDOM) {
				playerPlaysRandomly[r] = true;
			}
		}

		// Machine that takes care of the states extended to GDL-II
		stateMachine = new II_ProverStateMachine();
		stateMachine.initialize(match.getGame().getRules());
		currentState = stateMachine.getInitialState();
		System.out.println("Initial state is: " + currentState.toString());

		previousMoves = new ArrayList<Move>();

		percepts = new ArrayList<List<Percept>>();

		mostRecentErrors = new HashMap<Role, String>();

		match.appendState(currentState.getContents());

		observers = new ArrayList<Observer>();

		spectatorServerURL = null;
		forceUsingEntireClock = false;

	}

	public void startSavingToFilename(String theFilename) {
		saveToFilename = theFilename;
	}

	public String startPublishingToSpectatorServer(String theURL) {
		spectatorServerURL = theURL;
		return publishWhenNecessary();
	}

	@Override
	public void addObserver(Observer observer) {
		observers.add(observer);
	}

	public List<Integer> getGoals() throws GoalDefinitionException {
		List<Integer> goals = new ArrayList<Integer>();
		for (Role role : stateMachine.getRoles()) {
			goals.add(stateMachine.getGoal(currentState, role));
		}

		return goals;
	}

	public II_StateMachine getStateMachine() {
		return stateMachine;
	}

	@Override
	public void notifyObservers(Event event) {
		for (Observer observer : observers) {
			observer.observe(event);
		}

		// Add error events to mostRecentErrors for recording.
		if (event instanceof ServerIllegalMoveEvent) {
			ServerIllegalMoveEvent sEvent = (ServerIllegalMoveEvent) event;
			mostRecentErrors.put(sEvent.getRole(), "IL " + sEvent.getMove());

		} else if (event instanceof ServerTimeoutEvent) {
			ServerTimeoutEvent sEvent = (ServerTimeoutEvent) event;
			mostRecentErrors.put(sEvent.getRole(), "TO");

		} else if (event instanceof ServerConnectionErrorEvent) {
			ServerConnectionErrorEvent sEvent = (ServerConnectionErrorEvent) event;
			mostRecentErrors.put(sEvent.getRole(), "CE");
		}
	}

	// Should be called after each move, to collect all of the errors
	// caused by players and write them into the match description.
	private void appendErrorsToMatchDescription() {
		List<String> theErrors = new ArrayList<String>();
		for (int i = 0; i < stateMachine.getRoles().size(); i++) {
			Role r = stateMachine.getRoles().get(i);
			if (mostRecentErrors.containsKey(r)) {
				theErrors.add(mostRecentErrors.get(r));
			} else {
				theErrors.add("");
			}
		}
		match.appendErrors(theErrors);
		mostRecentErrors.clear();
	}

	@Override
	public void run() {
		try {
			int turn = 0;

			if (match.getPreviewClock() >= 0) {
				sendPreviewRequests();
			}

			notifyObservers(new ServerNewMatchEvent(stateMachine.getRoles(), currentState));
			notifyObservers(new ServerTimeEvent(match.getStartClock() * 1000));

			// Send the start requests. No need to change. No change in the protocol.
			System.out.println( "Sending start requests");
			sendStartRequests();

			appendErrorsToMatchDescription();

			// The game does not ends until a terminal state is reached.
			while (!stateMachine.isTerminal(currentState)) {

				publishWhenNecessary();
				saveWhenNecessary();
				notifyObservers(new ServerNewGameStateEvent(currentState));
				notifyObservers(new ServerTimeEvent(match.getPlayClock() * 1000));
				notifyObservers(new II_ServerMatchUpdatedEvent(match, spectatorServerKey, saveToFilename));

				// Gives back the players joint moves of the turn (X)
				System.out.println( "Sending play requests for turn: " + turn);
				previousMoves = sendPlayRequests(turn);
				System.out.println("Players moves: " + previousMoves);

				notifyObservers(new ServerNewMovesEvent(previousMoves));

				// Get the percepts of the current state (X) and applying the moves gotten from the players
				percepts = stateMachine.getPercepts(currentState,previousMoves);
				System.out.println( "Computed percepts according to the current state plus moves of the players: " + percepts);

				// Update current state to the next (X+1).
				currentState = stateMachine.getNextState(currentState, previousMoves);
				System.out.println( "Resulting state is: " + currentState);



				// Update the turn.
				turn++;
				System.out.println( "Updating turn to: " + turn);

				// Save match contents
				match.appendMoves2(previousMoves);
				match.appendPercepts2(percepts);
				match.appendState(currentState.getContents());
				appendErrorsToMatchDescription();

				if (match.isAborted()) {
					return;
				}
			}
			System.out.println( "Game Over!");
			match.markCompleted(stateMachine.getGoals(currentState));

			publishWhenNecessary();
			saveWhenNecessary();
			notifyObservers(new ServerNewGameStateEvent(currentState));
			notifyObservers(new ServerCompletedMatchEvent(getGoals()));
			notifyObservers(new II_ServerMatchUpdatedEvent(match, spectatorServerKey, saveToFilename));

			System.out.println( "Sending stop requests with turn: " + turn);
			sendStopRequests(turn, previousMoves, percepts);
		} catch (InterruptedException ie) {
			if (match.isAborted()) {
				return;
			} else {
				ie.printStackTrace();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void abort() {
		try {
			match.markAborted();
			sendAbortRequests();
			System.out.println( "Match Aborted!");
			saveWhenNecessary();
			publishWhenNecessary();
			notifyObservers(new ServerAbortedMatchEvent());
			notifyObservers(new II_ServerMatchUpdatedEvent(match, spectatorServerKey, saveToFilename));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void saveWhenNecessary() {
		if (saveToFilename == null) {
			return;
		}

		try {
			File file = new File(saveToFilename);
			if (!file.exists()) {
				file.createNewFile();
			}
			FileWriter fw = new FileWriter(file);
			BufferedWriter bw = new BufferedWriter(fw);
			bw.write(match.toJSON().toString());
			bw.close();
			fw.close();
		} catch (IOException ie) {
			ie.printStackTrace();
		}
	}

	private String publishWhenNecessary() {
		if (spectatorServerURL == null) {
			return null;
		}

		int nAttempt = 0;
		while (true) {
			try {
				spectatorServerKey = II_MatchPublisher.publishToSpectatorII_Server(spectatorServerURL, match);
				return spectatorServerKey;
			} catch (IOException e) {
				if (nAttempt > 9) {
					e.printStackTrace();
					return null;
				}
			}
			nAttempt++;
		}
	}

	public String getSpectatorServerKey() {
		return spectatorServerKey;
	}


	private synchronized List<Move> sendPlayRequests(int turn) throws InterruptedException, MoveDefinitionException {
		List<II_PlayRequestThread> threads = new ArrayList<II_PlayRequestThread>(hosts.size());
		for (int i = 0; i < hosts.size(); i++) {
			// alter this for GDL-III
			List<Move> legalMoves = stateMachine.getLegalMoves(currentState, stateMachine.getRoles().get(i));

			if (playerPlaysRandomly[i]) {
				threads.add(new II_RandomPlayRequestThread(match, turn, legalMoves));
			} else {

				if (previousMoves.isEmpty() && percepts.isEmpty()) {
					threads.add(new II_PlayRequestThread(this, match, turn, null, null, legalMoves,
							stateMachine.getRoles().get(i), hosts.get(i), ports.get(i),
							getPlayerNameFromMatchForRequest(i), playerGetsUnlimitedTime[i]));
				}

				else{
					threads.add(new II_PlayRequestThread(this, match, turn, previousMoves.get(i), percepts.get(i),
							legalMoves, stateMachine.getRoles().get(i), hosts.get(i), ports.get(i),
							getPlayerNameFromMatchForRequest(i), playerGetsUnlimitedTime[i]));
				}
			}
		}
		for (II_PlayRequestThread thread : threads) {
			thread.start();
		}

		// From here down everything checked
		if (forceUsingEntireClock) {
			Thread.sleep(match.getPlayClock() * 1000);
		}

		List<Move> moves = new ArrayList<Move>();
		// It is ordered
		for (II_PlayRequestThread thread : threads) {
			thread.join();
			moves.add(thread.getMove());
		}

		return moves;
	}

	private synchronized void sendPreviewRequests() throws InterruptedException {
		List<II_PreviewRequestThread> threads = new ArrayList<II_PreviewRequestThread>(hosts.size());
		for (int i = 0; i < hosts.size(); i++) {
			if (!playerPlaysRandomly[i]) {
				threads.add(new II_PreviewRequestThread(this, match, stateMachine.getRoles().get(i), hosts.get(i),
						ports.get(i), getPlayerNameFromMatchForRequest(i)));
			}
		}
		for (II_PreviewRequestThread thread : threads) {
			thread.start();
		}
		if (forceUsingEntireClock) {
			Thread.sleep(match.getStartClock() * 1000);
		}
		for (II_PreviewRequestThread thread : threads) {
			thread.join();
		}
	}

	private synchronized void sendStartRequests() throws InterruptedException {
		List<II_StartRequestThread> threads = new ArrayList<II_StartRequestThread>(hosts.size());
		for (int i = 0; i < hosts.size(); i++) {
			if (!playerPlaysRandomly[i]) {
				Role role = stateMachine.getRoles().get(i);
				threads.add(new II_StartRequestThread(this, match, role, hosts.get(i), ports.get(i),
						getPlayerNameFromMatchForRequest(i)));
			}
		}
		for (II_StartRequestThread thread : threads) {
			thread.start();
		}
		if (forceUsingEntireClock) {
			Thread.sleep(match.getStartClock() * 1000);
		}
		for (II_StartRequestThread thread : threads) {
			thread.join();
		}
	}

	// Need to change this --- Alter the protocol
	private synchronized void sendStopRequests(int turn, List<Move> previousMoves, List<List<Percept>> percepts)
			throws InterruptedException {
		List<II_StopRequestThread> threads = new ArrayList<II_StopRequestThread>(hosts.size());
		for (int i = 0; i < hosts.size(); i++) {
			if (!playerPlaysRandomly[i]) {
				threads.add(new II_StopRequestThread(this, match, turn, previousMoves.get(i), percepts.get(i),
						stateMachine.getRoles().get(i), hosts.get(i), ports.get(i),
						getPlayerNameFromMatchForRequest(i)));
			}
		}
		for (II_StopRequestThread thread : threads) {
			thread.start();
		}
		for (II_StopRequestThread thread : threads) {
			thread.join();
		}

	}

	private void sendAbortRequests() throws InterruptedException {
		List<II_AbortRequestThread> threads = new ArrayList<II_AbortRequestThread>(hosts.size());
		for (int i = 0; i < hosts.size(); i++) {
			if (!playerPlaysRandomly[i]) {
				threads.add(new II_AbortRequestThread(this, match, stateMachine.getRoles().get(i), hosts.get(i),
						ports.get(i), getPlayerNameFromMatchForRequest(i)));
			}
		}
		for (II_AbortRequestThread thread : threads) {
			thread.start();
		}
		for (II_AbortRequestThread thread : threads) {
			thread.join();
		}
		interrupt();
	}

	public void givePlayerUnlimitedTime(int i) {
		playerGetsUnlimitedTime[i] = true;
	}

	public void makePlayerPlayRandomly(int i) {
		playerPlaysRandomly[i] = true;
	}

	// Why would you want to force the game server to wait for the entire clock?
	// This can be used to rate-limit matches, so that you don't overload
	// supporting
	// services like the repository server, spectator server, players, etc.
	public void setForceUsingEntireClock() {
		forceUsingEntireClock = true;
	}

	public II_Match getMatch() {
		return match;
	}

	private String getPlayerNameFromMatchForRequest(int i) {
		if (match.getPlayerNamesFromHost() != null) {
			return match.getPlayerNamesFromHost().get(i);
		} else {
			return "";
		}
	}
}
