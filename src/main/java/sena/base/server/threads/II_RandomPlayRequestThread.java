package sena.base.server.threads;

import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

import org.ggp.base.util.statemachine.Move;

import sena.base.util.match.II_Match;

public class II_RandomPlayRequestThread extends II_PlayRequestThread {

	private Move move;

    public II_RandomPlayRequestThread(II_Match match, int turn, List<Move> legalMoves)
    {
        super(null, match, turn, null, null, legalMoves, null, null, 0, null, true);
        move = legalMoves.get(ThreadLocalRandom.current().nextInt(legalMoves.size()));
    }

    @Override
    public Move getMove()
    {
        return move;
    }

    @Override
    public void run()
    {
        ;
    }
}
