package sena.base.server.threads;

import org.ggp.base.server.request.RequestBuilder;
import org.ggp.base.util.statemachine.Role;

import sena.base.server.II_GameServer;
import sena.base.util.match.II_Match;

public final class II_StartRequestThread extends II_RequestThread {
	 public II_StartRequestThread(II_GameServer gameServer, II_Match match, Role role, String host, int port, String playerName)
	    {
	        super(gameServer, role, host, port, playerName, match.getStartClock() * 1000, RequestBuilder.getStartRequest(match.getMatchId(), role, match.getGame().getRules(), match.getStartClock(), match.getPlayClock(), match.getGdlScrambler()));
	    }

	    @Override
	    protected void handleResponse(String response) {
	        ;
	    }
}
