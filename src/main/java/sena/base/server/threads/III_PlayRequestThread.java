package sena.base.server.threads;


import java.util.HashSet;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

import org.ggp.base.server.event.ServerIllegalMoveEvent;
import org.ggp.base.util.gdl.factory.GdlFactory;
import org.ggp.base.util.gdl.factory.exceptions.GdlFormatException;
import org.ggp.base.util.statemachine.Move;
import org.ggp.base.util.statemachine.Role;
import org.ggp.base.util.symbol.factory.exceptions.SymbolFormatException;

import sena.base.server.III_GameServer;
import sena.base.server.request.II_RequestBuilder;
import sena.base.util.match.II_Match;
import sena.base.util.statemachine.Percept;

public class III_PlayRequestThread extends III_RequestThread {

	private final III_GameServer gameServer;

	// Do not need to change this legalMoves != previousMoves
	private final List<Move> legalMoves;
	private final II_Match match;
	private final Role role;

	private Move move;

	public III_PlayRequestThread(III_GameServer gameServer, II_Match match,int turn, Move previousMove, List<Percept> percepts,
			List<Move> legalMoves, Role role, String host, int port, String playerName, boolean unlimitedTime) {

		super(gameServer, role, host, port, playerName, unlimitedTime ? -1 : (match.getPlayClock() * 1000 + 1000),
				II_RequestBuilder.getPlayRequest(match.getMatchId(), turn, previousMove, percepts, match.getGdlScrambler()));


    	this.gameServer = gameServer;
		this.legalMoves = legalMoves;
		this.match = match;
		this.role = role;

		move = legalMoves.get(ThreadLocalRandom.current().nextInt(legalMoves.size()));
	}

	public Move getMove() {
		return move;
	}

	// Do not think this needs changes
	@Override
	protected void handleResponse(String response) {
		try {
			Move candidateMove = gameServer.getStateMachine()
					.getMoveFromTerm(GdlFactory.createTerm(match.getGdlScrambler().unscramble(response).toString()));
			if (new HashSet<Move>(legalMoves).contains(candidateMove)) {
				move = candidateMove;
			} else {
				System.out.println( "Illegal move! " + candidateMove + "for role: " + role + "does not belongs in: " + legalMoves);
				gameServer.notifyObservers(new ServerIllegalMoveEvent(role, candidateMove));
			}
		} catch (GdlFormatException e) {
			gameServer.notifyObservers(new ServerIllegalMoveEvent(role, null));
		} catch (SymbolFormatException e) {
			gameServer.notifyObservers(new ServerIllegalMoveEvent(role, null));
		}
	}
}
