package sena.base.server.threads;

import java.io.IOException;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;

import org.ggp.base.server.event.ServerConnectionErrorEvent;
import org.ggp.base.server.event.ServerTimeoutEvent;
import org.ggp.base.util.statemachine.Role;

import sena.base.server.III_GameServer;
import sena.base.util.http.II_HttpRequest;

public abstract class III_RequestThread extends Thread{

	private final III_GameServer gameServer;
    private final String host;
    private final int port;
    private final String playerName;
    private final int timeout;
    private final Role role;
    private final String request;

    public III_RequestThread(III_GameServer gameServer, Role role, String host, int port, String playerName, int timeout, String request)
    {
        this.gameServer = gameServer;
        this.role = role;
        this.host = host;
        this.port = port;
        this.playerName = playerName;
        this.timeout = timeout;
        this.request = request;

    }

    protected abstract void handleResponse(String response);

    @Override
    public void run()
    {
        try {


            String response = II_HttpRequest.issueRequest(host, port, playerName, request, timeout);
            handleResponse(response);
        } catch (SocketTimeoutException e) {
            gameServer.notifyObservers(new ServerTimeoutEvent(role));
        } catch (UnknownHostException e) {
            gameServer.notifyObservers(new ServerConnectionErrorEvent(role));
        } catch (IOException e) {
            gameServer.notifyObservers(new ServerConnectionErrorEvent(role));
        }
    }
}
