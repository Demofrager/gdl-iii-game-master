package sena.base.server.event;

import org.ggp.base.util.observer.Event;

import sena.base.util.match.II_Match;

public class II_ServerMatchUpdatedEvent extends Event{
	 private final II_Match match;
	    private final String externalPublicationKey;
	    private final String externalFilename;

	    public II_ServerMatchUpdatedEvent(II_Match match, String externalPublicationKey, String externalFilename) {
	        this.match = match;
	        this.externalFilename = externalFilename;
	        this.externalPublicationKey = externalPublicationKey;
	    }

	    public II_Match getMatch() {
	        return match;
	    }

	    public String getExternalFilename() {
	        return externalFilename;
	    }

	    public String getExternalPublicationKey() {
	        return externalPublicationKey;
	    }
}
