package sena.base.player.request.grammar;


import java.util.ArrayList;
import java.util.List;

import org.ggp.base.player.event.PlayerTimeEvent;
import org.ggp.base.player.gamer.event.GamerUnrecognizedMatchEvent;
import org.ggp.base.player.gamer.exception.MoveSelectionException;
import org.ggp.base.player.request.grammar.Request;
import org.ggp.base.util.gdl.factory.GdlFactory;
import org.ggp.base.util.gdl.grammar.GdlTerm;
import org.ggp.base.util.logging.GamerLogger;
import org.ggp.base.util.symbol.factory.exceptions.SymbolFormatException;

import sena.base.player.gamer.II_Gamer;


public final class II_PlayRequest extends Request
{
    private final II_Gamer gamer;
    private final String matchId;
    private final String turn;
    private final GdlTerm move;
    private final List<GdlTerm>percepts;

    public II_PlayRequest(II_Gamer gamer, String matchId, String turn, GdlTerm move, List<GdlTerm> percepts)
    {
        this.gamer = gamer;
        this.matchId = matchId;
        this.turn = turn;
        this.move = move;
        this.percepts = percepts;
    }

    @Override
    public String getMatchId() {
        return matchId;
    }

    @Override
    public String process(long receptionTime)
    {
        // First, check to ensure that this play request is for the match
        // we're currently playing. If we're not playing a match, or we're
        // playing a different match, send back "busy".
        if (gamer.getMatch() == null || !gamer.getMatch().getMatchId().equals(matchId)) {
            gamer.notifyObservers(new GamerUnrecognizedMatchEvent(matchId));
            GamerLogger.logError("GamePlayer", "Got play message not intended for current game: ignoring.");
            return "busy";
        }
        try {
        if (move != null) {
        	List<GdlTerm> moves = new ArrayList<GdlTerm>(2); // Sending Moves List as: <MyMove, OtherPlayerMove>
        	GdlTerm otherPlayerMove = GdlFactory.createTerm("II_Game_No_OpponentMove");
        	moves.add(move);
        	moves.add(otherPlayerMove);
            gamer.getMatch().appendMoves(moves);
        }

        if (percepts != null){
        	List<List<GdlTerm>>perceptList = new ArrayList<List<GdlTerm>>(2); // Sending Moves List as: <MyPercepts, OtherPlayerPercepts>
        	List<GdlTerm> otherPlayerPercepts = new ArrayList<GdlTerm>(1); // Other player percepts:
        	GdlTerm otherPlayerPercept = GdlFactory.createTerm("II_Game_No_OpponentPercepts");
        	otherPlayerPercepts.add(otherPlayerPercept);
        	perceptList.add(percepts);
        	perceptList.add(otherPlayerPercepts);
        	gamer.getMatch().appendPercepts(perceptList);
        }

            gamer.notifyObservers(new PlayerTimeEvent(gamer.getMatch().getPlayClock() * 1000));
            return gamer.selectMove(gamer.getMatch().getPlayClock() * 1000 + receptionTime).toString();

        } catch (MoveSelectionException e) {
            GamerLogger.logStackTrace("GamePlayer", e);
            return "nil";

        } catch (SymbolFormatException e) {
        	 GamerLogger.logStackTrace("GamePlayer", e);
        	 return "SymbolException";
		}
    }

    @Override
    public String toString()
    {
        return "play";
    }
}