package sena.base.player.request.grammar;

import org.ggp.base.player.request.grammar.Request;
import org.ggp.base.util.presence.InfoResponse;

import sena.base.player.gamer.II_Gamer;

public final class II_InfoRequest extends Request
{
    private final II_Gamer gamer;

    public II_InfoRequest(II_Gamer gamer)
    {
        this.gamer = gamer;
    }

    @Override
    public String getMatchId() {
        return null;
    }

    @Override
    public String process(long receptionTime)
    {
        InfoResponse info = new InfoResponse();
        info.setName(gamer.getName());
        info.setStatus(gamer.getMatch() == null ? "available" : "busy");
        info.setSpecies(gamer.getSpecies());
        return info.toSymbol().toString();
    }

    @Override
    public String toString()
    {
        return "info";
    }
}