package sena.base.player.request.factory;

import java.util.ArrayList;
import java.util.List;

import org.ggp.base.player.request.factory.exceptions.RequestFormatException;
import org.ggp.base.player.request.grammar.Request;
import org.ggp.base.util.game.Game;
import org.ggp.base.util.gdl.factory.GdlFactory;
import org.ggp.base.util.gdl.factory.exceptions.GdlFormatException;
import org.ggp.base.util.gdl.grammar.GdlConstant;
import org.ggp.base.util.gdl.grammar.GdlTerm;
import org.ggp.base.util.symbol.factory.SymbolFactory;
import org.ggp.base.util.symbol.grammar.Symbol;
import org.ggp.base.util.symbol.grammar.SymbolAtom;
import org.ggp.base.util.symbol.grammar.SymbolList;

import sena.base.player.gamer.II_Gamer;
import sena.base.player.request.grammar.II_AbortRequest;
import sena.base.player.request.grammar.II_InfoRequest;
import sena.base.player.request.grammar.II_PlayRequest;
import sena.base.player.request.grammar.II_PreviewRequest;
import sena.base.player.request.grammar.II_StartRequest;
import sena.base.player.request.grammar.II_StopRequest;

public final class II_RequestFactory {
	public Request create(II_Gamer gamer, String source) throws RequestFormatException {
		try {
			SymbolList list = (SymbolList) SymbolFactory.create(source);
			SymbolAtom head = (SymbolAtom) list.get(0);

			String type = head.getValue().toLowerCase();
			if (type.equals("play")) {
				return createPlay(gamer, list);
			} else if (type.equals("start")) {
				return createStart(gamer, list);
			} else if (type.equals("stop")) {
				return createStop(gamer, list);
			} else if (type.equals("abort")) {
				return createAbort(gamer, list);
			} else if (type.equals("info")) {
				return createInfo(gamer, list);
			} else if (type.equals("preview")) {
				return createPreview(gamer, list);
			} else {
				throw new IllegalArgumentException("Unrecognized request type!");
			}
		} catch (Exception e) {
			throw new RequestFormatException(source, e);
		}
	}

	private II_PlayRequest createPlay(II_Gamer gamer, SymbolList list) throws GdlFormatException {
		if (list.size() != 5) {
			throw new IllegalArgumentException("Expected exactly 5 arguments!");
		}

		SymbolAtom arg1 = (SymbolAtom) list.get(1);
		SymbolAtom arg2 = (SymbolAtom) list.get(2);

		Symbol arg3 = list.get(3);
		Symbol arg4 = list.get(4);

		String matchId = arg1.getValue();
		String turn = arg2.getValue();
		GdlTerm move = parseMoves(arg3);
		List<GdlTerm> percepts = parsePercepts(arg4);
		return new II_PlayRequest(gamer, matchId, turn, move, percepts);
	}

	private II_StartRequest createStart(II_Gamer gamer, SymbolList list) throws GdlFormatException {
		if (list.size() < 6) {
			throw new IllegalArgumentException("Expected at least 5 arguments!");
		}

		SymbolAtom arg1 = (SymbolAtom) list.get(1);
		SymbolAtom arg2 = (SymbolAtom) list.get(2);
		SymbolList arg3 = (SymbolList) list.get(3);
		SymbolAtom arg4 = (SymbolAtom) list.get(4);
		SymbolAtom arg5 = (SymbolAtom) list.get(5);

		String matchId = arg1.getValue();
		GdlConstant roleName = (GdlConstant) GdlFactory.createTerm(arg2);
		String theRulesheet = arg3.toString();
		int startClock = Integer.valueOf(arg4.getValue());
		int playClock = Integer.valueOf(arg5.getValue());

		// For now, there are only five standard arguments. If there are any
		// new standard arguments added to START, they should be added here.

		Game theReceivedGame = Game.createEphemeralGame(theRulesheet);
		return new II_StartRequest(gamer, matchId, roleName, theReceivedGame, startClock, playClock);
	}

	private II_StopRequest createStop(II_Gamer gamer, SymbolList list) throws GdlFormatException {
		if (list.size() != 5) {
			throw new IllegalArgumentException("Expected exactly 5 arguments!");
		}

		SymbolAtom arg1 = (SymbolAtom) list.get(1);
		SymbolAtom arg2 = (SymbolAtom) list.get(2);

		Symbol arg3 = list.get(3);
		Symbol arg4 = list.get(4);

		String matchId = arg1.getValue();
		String turn = arg2.getValue();
		GdlTerm move = parseMoves(arg3);
		List<GdlTerm> percepts = parsePercepts(arg4);

		return new II_StopRequest(gamer, matchId, turn, move, percepts);
	}

	private II_AbortRequest createAbort(II_Gamer gamer, SymbolList list) throws GdlFormatException {
		if (list.size() != 2) {
			throw new IllegalArgumentException("Expected exactly 1 argument!");
		}

		SymbolAtom arg1 = (SymbolAtom) list.get(1);
		String matchId = arg1.getValue();

		return new II_AbortRequest(gamer, matchId);
	}

	private II_InfoRequest createInfo(II_Gamer gamer, SymbolList list) throws GdlFormatException {
		if (list.size() != 1) {
			throw new IllegalArgumentException("Expected no arguments!");
		}

		return new II_InfoRequest(gamer);
	}

	private II_PreviewRequest createPreview(II_Gamer gamer, SymbolList list) throws GdlFormatException {
		if (list.size() != 3) {
			throw new IllegalArgumentException("Expected exactly 2 arguments!");
		}

		SymbolAtom arg1 = (SymbolAtom) list.get(1);
		SymbolAtom arg2 = (SymbolAtom) list.get(2);

		String theRulesheet = arg1.toString();
		int previewClock = Integer.valueOf(arg2.getValue());

		Game theReceivedGame = Game.createEphemeralGame(theRulesheet);
		return new II_PreviewRequest(gamer, theReceivedGame, previewClock);
	}

	private GdlTerm parseMoves(Symbol symbol) throws GdlFormatException {

		GdlTerm move = GdlFactory.createTerm(symbol);

		if (move.toString().equals("NIL")) {
			return null;
		} else {
			return move;
		}
	}

	private List<GdlTerm> parsePercepts(Symbol symbol) throws GdlFormatException {

		if (symbol instanceof SymbolAtom) {
			System.out.println("It's a symbol Atom!: " + symbol);
			return null;
		} else {
			List<GdlTerm> percepts = new ArrayList<GdlTerm>();
			SymbolList list = (SymbolList) symbol;

			for (int i = 0; i < list.size(); i++) {
				percepts.add(GdlFactory.createTerm(list.get(i)));
			}

			return percepts;
		}
	}
}