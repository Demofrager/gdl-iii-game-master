package sena.base.knowledge;

import org.ggp.base.util.gdl.grammar.GdlSentence;
import org.ggp.base.util.gdl.grammar.GdlTerm;

public class CommonKnowledge extends KnowledgeRule {

	private GdlTerm target;
	public CommonKnowledge(GdlSentence groundRule) {
		super(groundRule);
		target = groundRule.get(0);
	}

	public GdlTerm getTerm(){
		return target;
	}
}
