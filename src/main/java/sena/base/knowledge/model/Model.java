package sena.base.knowledge.model;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import org.ggp.base.util.statemachine.MachineState;
import org.ggp.base.util.statemachine.Move;

import sena.base.util.statemachine.Percept;

/** A model is a state and his identification (full path)
 *
 * @author demofrager
 *
 */
public class Model {

	private MachineState model;
	private List<List<Move>> modelMovePath;
	private List<List<List<Percept>>> modelPerceptPath;

	public Model(MachineState model) {
		this.model = model;
		modelMovePath = new LinkedList<List<Move>>();
		modelPerceptPath = new LinkedList<List<List<Percept>>>();
	}

	public Model(MachineState model, List<List<Move>> modelID, List<List<List<Percept>>> modelPerceptHistory) {
		this.model = model;
		this.modelMovePath = new ArrayList<>(modelID);
		modelPerceptPath = modelPerceptHistory;
	}

	public MachineState getModel() {
		return model;
	}

	public List<List<Move>> getModelID() {
		return modelMovePath;
	}

	public List<List<List<Percept>>> getModelPerceptHistory(){
		return modelPerceptPath;
	}



	public void setModel(MachineState model) {
		this.model = model;
	}

	public void setModelID(List<List<Move>> modelID) {
		this.modelMovePath = modelID;
	}

	public void setModelPerceptHistory(List<List<List<Percept>>> modelPerceptPath){
		this.modelPerceptPath = modelPerceptPath;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Model other = (Model) obj;
		if (modelMovePath == null) {
			if (other.modelMovePath != null)
				return false;
		} else if (!modelMovePath.equals(other.modelMovePath))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Model {content=" + model + ", path=" + modelMovePath + ", perceptsPath=" + modelPerceptPath + "}\n";
	}
}
