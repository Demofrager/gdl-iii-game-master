package sena.base.knowledge.indistinguishability;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Stack;

import javax.naming.SizeLimitExceededException;

import org.ggp.base.util.gdl.grammar.GdlPool;
import org.ggp.base.util.statemachine.Move;
import org.ggp.base.util.statemachine.Role;

import sena.base.knowledge.model.Model;
import sena.base.util.path.PathUtilities;
import sena.base.util.statemachine.Percept;

public class AccessabilityRelationManager {

	// for knowledge
	private List<Map<Role, Matrix>> knowledgeHistory;

	private Map<Role, Matrix> accessabilityRelations;
	private int numberOfModels;
	private List<Role> roles;

	public AccessabilityRelationManager(List<Role> roles, int numberOfModels) {
		knowledgeHistory = new LinkedList<Map<Role, Matrix>>();

		accessabilityRelations = new HashMap<Role, Matrix>(roles.size());
		this.roles = roles;
		this.numberOfModels = numberOfModels;

		for (Role role : roles)
			accessabilityRelations.put(role, new Matrix(numberOfModels));
	}

	/**
	 * This method goes through our player's matrices to find the models which
	 * are inconsistent. By inconsistent we mean that, according to the
	 * semantics of Dynamic Epistemic Logic and GDL the model is now longer
	 * achievable by any of the players in the game, thus useless.
	 *
	 * A model is inconsistent if is neither ~ with the actual play of the game
	 * nor any other model.
	 *
	 * @return a list of model id's that are inconsistent with the game rules
	 */
	public List<Integer> getAllTransitiveClousureUselessModels() {

		LinkedList<Integer> toReturn = new LinkedList<Integer>();

		// Maybe another solution to avoid code redundancy
		Set<Integer> discovered = new HashSet<>(numberOfModels);
		Stack<Integer> s = new Stack<Integer>();
		Matrix m = null;

		for (int modelIndex = 0; modelIndex < numberOfModels; modelIndex++) {//Depth-first-search
			s.push(modelIndex);

			while (!s.isEmpty()) {
				int index = s.pop();
				if (!discovered.contains(index)) {
					discovered.add(index);

					// Check edges
					for (Role role : roles) {
						if (!role.getName().equals(GdlPool.RANDOM)) {
							m = accessabilityRelations.get(role);
							for (int i : m.getAllIndistinguishableModelsIds(index)) {
								s.push(i);
							}
						}
					}
				}
			}

			boolean consistent = false;
			for(int i : discovered){
				// Check if any of the models is connected to the actual play of the game
				for (Role role : roles) {
					if (!role.getName().equals(GdlPool.RANDOM)) {
						m = accessabilityRelations.get(role);
						consistent |= m.getUsefulNess(i);
					}
				}
			}

			if(!consistent) toReturn.add(modelIndex);
			discovered = new HashSet<>(numberOfModels);
			s = new Stack<Integer>();
		}

		return toReturn;
	}

	public void updateCurrentAccessabilityRelation(List<List<Move>> actualMoveHistory,
			List<List<List<Percept>>> actualPerceptHistory, List<Model> models) {

		int roleID = 0;
		for (Role role : roles) {
			if (!role.getName().equals(GdlPool.RANDOM)) {
				roleID = getRoleID(role);
				updateCurrentAccessabilityRelation(role, PathUtilities.movePathSplitter(actualMoveHistory, roleID),
						PathUtilities.perceptsPathSplitter(actualPerceptHistory, roleID), models);
			}

		}
	}

	public void updateCurrentAccessabilityRelation(Role role, List<Move> hisMoveHistory,
			List<List<Percept>> hisPerceptsHistory, List<Model> models) {
		Matrix indMatrix = accessabilityRelations.get(role);
		Model toCheck;
		List<Move> partialModelPath;
		List<List<Percept>> partialModelPercepts;
		int roleID = getRoleID(role);

		for (int i = 0; i < models.size(); i++) {
			toCheck = models.get(i);
			partialModelPath = PathUtilities.movePathSplitter(toCheck.getModelID(), roleID);
			partialModelPercepts = PathUtilities.perceptsPathSplitter(toCheck.getModelPerceptHistory(), roleID);

			// If player has made the same moves in the model and the actual
			// game path,
			// then we are allowed to go in and see if he can distinguish
			// between any
			// other model. Otherwise the model is distinguishable.
			if (hisMoveHistory.equals(partialModelPath) && hisPerceptsHistory.equals(partialModelPercepts))
				indMatrix.markUseful(i);
			else
				indMatrix.markUseless(i);

			Model toCompare;
			List<Move> partialModelPathToCompare;
			List<List<Percept>> partialModelPerceptsToCompare;
			for (int j = i; j < models.size(); j++) {
				if (j != i) { // A model is always indistinguishable between
								// itself
								// it makes more sense in my head to do it like
								// this
					toCompare = models.get(j);
					partialModelPathToCompare = PathUtilities.movePathSplitter(toCompare.getModelID(), roleID);
					partialModelPerceptsToCompare = PathUtilities
							.perceptsPathSplitter(toCompare.getModelPerceptHistory(), roleID);
					if (partialModelPath.equals(partialModelPathToCompare)
							&& partialModelPercepts.equals(partialModelPerceptsToCompare))
						indMatrix.markIndistinguishable(i, j);
					else
						indMatrix.unmarkIndistinguishable(i, j);
				}
			}
		}
	}

	public Map<Role, Matrix> getCurrentMatrices() {
		return accessabilityRelations;
	}

	public Matrix getCurrentMatrix(Role role) {
		return accessabilityRelations.get(role);
	}

	public Map<Role, Matrix> getSnapshot(int turn) {
		return knowledgeHistory.get(turn);
	}

	public List<Map<Role, Matrix>> getSnapshotHistory(int turn) {

		List<Map<Role, Matrix>> toReturn = new ArrayList<>(turn);

		for (int i = 0; i < turn; i++)
			toReturn.add(getSnapshot(i));

		return toReturn;
	}

	public void takeSnapshot() {
		knowledgeHistory.add(new HashMap<Role, Matrix>(accessabilityRelations));
	}

	private int getRoleID(Role role) {
		int roleID = 0;
		for (int i = 0; i < roles.size(); i++) {
			if (roles.get(i).equals(role))
				roleID = i;
		}
		return roleID;
	}

	@Override
	public String toString() {
		Matrix m;
		StringBuilder sb = new StringBuilder();
		for (Role role : roles) {
			if (!role.getName().equals(GdlPool.RANDOM)) {
				m = accessabilityRelations.get(role);
				sb.append("For the role: " + role.toString() + "\n");
				sb.append(m.toString() + "\n");
			}
		}

		return sb.toString();
	}


	public void updateAccessabilityRelation(List<List<Move>> actualMoveHistory,
			List<List<List<Percept>>> actualPerceptHistory, int level, List<Model> models) throws SizeLimitExceededException {

		int roleID = 0;
		for (Role role : roles) {
			if (!role.getName().equals(GdlPool.RANDOM)) {
				roleID = getRoleID(role);
				updateAccessabilityRelation(role, PathUtilities.movePathSplitter(actualMoveHistory, roleID, level),
						PathUtilities.perceptsPathSplitter(actualPerceptHistory, roleID, level), level, models);
			}

		}
	}

	private void updateAccessabilityRelation(Role role, List<Move> movePathSplitter,
			List<List<Percept>> perceptsPathSplitter, int level, List<Model> models) throws SizeLimitExceededException {

		Matrix indMatrix = getSnapshot(level).get(role);
		Model toCheck;
		List<Move> partialModelPath;
		List<List<Percept>> partialModelPercepts;
		int roleID = getRoleID(role);

		for (int i = 0; i < models.size(); i++) {
			toCheck = models.get(i);
			partialModelPath = PathUtilities.movePathSplitter(toCheck.getModelID(), roleID, level);
			partialModelPercepts = PathUtilities.perceptsPathSplitter(toCheck.getModelPerceptHistory(), roleID,level);

			// If player has made the same moves in the model and the actual game path,
			// then we are allowed to go in and see if he can distinguish between any
			// other model. Otherwise the model is distinguishable.
			if (movePathSplitter.equals(partialModelPath) && perceptsPathSplitter.equals(partialModelPercepts))
				indMatrix.markUseful(i);
			else
				indMatrix.markUseless(i);

			Model toCompare;
			List<Move> partialModelPathToCompare;
			List<List<Percept>> partialModelPerceptsToCompare;
			for (int j = i; j < models.size(); j++) {
				if (j != i) { // A model is always indistinguishable between itself
							  // it makes more sense in my head to do it like this
					toCompare = models.get(j);
					partialModelPathToCompare = PathUtilities.movePathSplitter(toCompare.getModelID(), roleID, level);
					partialModelPerceptsToCompare = PathUtilities.perceptsPathSplitter(toCompare.getModelPerceptHistory(), roleID, level);
					if (partialModelPath.equals(partialModelPathToCompare)
							&& partialModelPercepts.equals(partialModelPerceptsToCompare))
						indMatrix.markIndistinguishable(i, j);
					else
						indMatrix.unmarkIndistinguishable(i, j);
				}
			}
		}
	}

	public List<Integer> getAllTransitiveClousureUsefulModels() {

		Set<Integer> aux = new HashSet<Integer>();

		// Maybe another solution to avoid code redundancy
		Set<Integer> discovered = new HashSet<>(numberOfModels);
		Stack<Integer> s = new Stack<Integer>();
		Matrix m = null;

		for (int modelIndex = 0; modelIndex < numberOfModels; modelIndex++) {//Depth-first-search
			s.push(modelIndex);

			while (!s.isEmpty()) {
				int index = s.pop();
				if (!discovered.contains(index)) {
					discovered.add(index);

					// Check edges
					for (Role role : roles) {
						if (!role.getName().equals(GdlPool.RANDOM)) {
							m = accessabilityRelations.get(role);
							for (int i : m.getAllIndistinguishableModelsIds(index)) {
								s.push(i);
							}
						}
					}
				}
			}

			boolean consistent = false;
			for(int i : discovered){
				// Check if any of the models is connected to the actual play of the game
				for (Role role : roles) {
					if (!role.getName().equals(GdlPool.RANDOM)) {
						m = accessabilityRelations.get(role);
						consistent |= m.getUsefulNess(i);
					}
				}
			}

			if(consistent) aux.add(modelIndex);
			discovered = new HashSet<>(numberOfModels);
			s = new Stack<Integer>();
		}

		return new ArrayList<>(aux);

	}

}
