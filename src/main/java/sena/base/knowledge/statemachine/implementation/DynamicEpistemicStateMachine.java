package sena.base.knowledge.statemachine.implementation;

import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import javax.naming.SizeLimitExceededException;

import org.ggp.base.util.gdl.grammar.GdlRelation;
import org.ggp.base.util.gdl.grammar.GdlSentence;
import org.ggp.base.util.gdl.grammar.GdlTerm;
import org.ggp.base.util.statemachine.MachineState;
import org.ggp.base.util.statemachine.Move;
import org.ggp.base.util.statemachine.Role;
import org.ggp.base.util.statemachine.exceptions.MoveDefinitionException;
import org.ggp.base.util.statemachine.exceptions.TransitionDefinitionException;

import sena.base.knowledge.CommonKnowledge;
import sena.base.knowledge.KnowledgeRule;
import sena.base.knowledge.PersonalKnowledge;
import sena.base.knowledge.bag.exception.InconsistentPathToCurrentLvlException;
import sena.base.knowledge.bag.exception.ModelNotFoundException;
import sena.base.knowledge.indistinguishability.AccessabilityRelationManager;
import sena.base.knowledge.indistinguishability.Matrix;
import sena.base.knowledge.model.Model;
import sena.base.knowledge.sampler.DynamicEpistemicSampler;
import sena.base.knowledge.sampler.implementation.PerspectiveShiftingDynamicEpistemicSampler;
import sena.base.knowledge.sampler.implementation.RandomEpistemicSampler;
import sena.base.knowledge.statemachine.KnowledgeStateMachine;
import sena.base.util.statemachine.III_StateMachine;
import sena.base.util.statemachine.Percept;
import sena.base.util.statemachine.exceptions.PerceptDefinitionException;
import sena.base.util.statemachine.implementation.prover.III_ProverStateMachine;

public class DynamicEpistemicStateMachine extends KnowledgeStateMachine {

	private int NUMBER_OF_MODELS = 50;
	private int INCONSISTENT_MODELS = 40;
	//private static final int NUMBER_OF_MODELS = 100;
	//private static final int NUMBER_OF_MODELS = 150;

	private static final boolean PERSPECTIVE_SHIFTING_SAMPLING = true;

	// For knowledge terms calculation
	private Set<KnowledgeRule> targetKnowsRules;

	private AccessabilityRelationManager indistinguishabilityManager;
	private DynamicEpistemicSampler sampler;

	private III_StateMachine stateMachine;
	private MachineState currentState;

	private List<List<Move>> actualMoveHistory;
	private List<List<List<Percept>>> actualPerceptHistory;

	public DynamicEpistemicStateMachine(III_ProverStateMachine stateMachine, List<Role> roles,
			MachineState initialState, int nrModels, int inconsistent) {

		this.stateMachine = stateMachine;
		this.NUMBER_OF_MODELS = nrModels;
		this.INCONSISTENT_MODELS = inconsistent;

		currentState = initialState.clone();


		indistinguishabilityManager = new AccessabilityRelationManager(roles, NUMBER_OF_MODELS);

		if (PERSPECTIVE_SHIFTING_SAMPLING) {
			sampler = new PerspectiveShiftingDynamicEpistemicSampler(stateMachine, roles, NUMBER_OF_MODELS,
					initialState);
		} else
			sampler = new RandomEpistemicSampler(stateMachine, roles, NUMBER_OF_MODELS, initialState);

		actualMoveHistory = new LinkedList<List<Move>>();
		actualPerceptHistory = new LinkedList<List<List<Percept>>>();
	}

	@Override
	public void initialize(Set<KnowledgeRule> targetKnowsRules) {
		this.targetKnowsRules = targetKnowsRules;

		sampler.initialize(targetKnowsRules);
		//System.out.println(this.targetKnowsRules);
	}


	@Override
	public void updateKnowledgeStage( List<Move> previousMoves,
			List<List<Percept>> percepts) throws MoveDefinitionException, PerceptDefinitionException,
			TransitionDefinitionException, SizeLimitExceededException {

		actualMoveHistory.add(previousMoves);
		actualPerceptHistory.add(percepts);

		// Change this to a global variable
		List<Integer> inconsistentModelsIDs = new LinkedList<Integer>();
		try {
			//System.out.println("Actual Joint Move: " + previousMoves);
			//System.out.println("Actual Joint Percepts: " + percepts);

			// System.out.println("Number of transitive closure inconsistent
			// models: " + inconsistentModelsIDs.size());
			// System.out.println(inconsistentModelsIDs);

			indistinguishabilityManager.updateCurrentAccessabilityRelation(actualMoveHistory, actualPerceptHistory,
					sampler.sampleNextLvl(previousMoves, inconsistentModelsIDs));

			indistinguishabilityManager.takeSnapshot();

			// System.out.println(sampler.getBag());
			// System.out.println(indManager.toString());
			sampler.updateModelsEpistemicState(indistinguishabilityManager.getCurrentMatrices());
			// System.out.println(sampler.getBag());

			inconsistentModelsIDs = indistinguishabilityManager.getAllTransitiveClousureUselessModels();

			// System.out.println("Number of total models: " +
			// NUMBER_OF_MODELS);
			// System.out.println("Number of transitive closure inconsistent
			// models: " + inconsistentModelsIDs.size());
			// System.out.println(inconsistentModelsIDs);

			//while (inconsistentModelsIDs.size() > 10) {
			//while (inconsistentModelsIDs.size() > 25) {
			while (inconsistentModelsIDs.size() > INCONSISTENT_MODELS)  {

				// System.out.println("Re sampling");
				resample(inconsistentModelsIDs, actualMoveHistory);
				//System.out.println(sampler.getBag());
				// indManager.updateAccessabilityRelation(actualMoveHistory,
				// actualPerceptHistory,
				// sampler.getBag().getAll());
				inconsistentModelsIDs = indistinguishabilityManager.getAllTransitiveClousureUselessModels();
				// System.out.println(indManager.toString());
				// System.out.println("Number of total models: " +
				// NUMBER_OF_MODELS);
				// System.out.println("Number of transitive closure inconsistent models: " + inconsistentModelsIDs.size());
				// System.out.println(inconsistentModelsIDs);

			}

			// System.out.println(indManager.toString());
			// System.out.println(sampler.getBag());
			// System.out.println("Number of transitive closure inconsistent
			// models: " + inconsistentModelsIDs.size());
			// System.out.println(inconsistentModelsIDs);
		} catch (ModelNotFoundException e) {
			e.printStackTrace();
		} catch (InconsistentPathToCurrentLvlException e) {
			e.printStackTrace();
		}
	}

	private void resample(List<Integer> inconsistentModelsIDs, List<List<Move>> actualMoveHistory)
			throws ModelNotFoundException, InconsistentPathToCurrentLvlException, MoveDefinitionException,
			TransitionDefinitionException, PerceptDefinitionException, SizeLimitExceededException {

		int samlerLvl = sampler.getSamplerLvl();
		// Start sampling from the top until the current state
		for (int currentResampleLvl = 0; currentResampleLvl < samlerLvl; currentResampleLvl++) {
			// System.out.println("Current level:" + currentResampleLvl);
			resample(inconsistentModelsIDs, currentResampleLvl, actualMoveHistory.get(currentResampleLvl));
			// System.out.println(modelSampler.getBag());

			// Possibly error here
			indistinguishabilityManager.updateAccessabilityRelation(actualMoveHistory, actualPerceptHistory, currentResampleLvl,
					sampler.getBag().getAll());
			// System.out.println(indManager.getSnapshot(currentResampleLvl));

			// And here.
			sampler.updateModelsEpistemicState(indistinguishabilityManager.getSnapshot(currentResampleLvl), inconsistentModelsIDs);
			// Seems like some inconsistency with the current re sample level
		}

	}

	private List<Model> resample(List<Integer> inconsistentModelsIDs, int level, List<Move> leveledJointMove)
			throws ModelNotFoundException, InconsistentPathToCurrentLvlException, MoveDefinitionException,
			TransitionDefinitionException, PerceptDefinitionException {

		return sampler.resample(inconsistentModelsIDs, level, leveledJointMove);
	}

	@Override
	public Set<GdlSentence> getKnowledgeStage() throws ModelNotFoundException, TransitionDefinitionException {
		Set<GdlSentence> knowledgeStage = new HashSet<GdlSentence>();
		for (KnowledgeRule knows : targetKnowsRules) {
			if (knows instanceof PersonalKnowledge) {
				PersonalKnowledge target = (PersonalKnowledge) knows;
				if (verifyKnowledge(target))
					knowledgeStage.add(target.getSentence());
			} else {
				CommonKnowledge target = (CommonKnowledge) knows;
				if (verifyKnowledge(target))
					knowledgeStage.add(target.getSentence());
			}
		}

		// update the actual state of the game
		currentState = stateMachine.getNextState(currentState, actualMoveHistory.get(actualMoveHistory.size() - 1));
		return knowledgeStage;
	}

	/**
	 * Applies the semantics of the personal knowledge target.
	 * Using the current matrices.
	 */
	private boolean verifyKnowledge(PersonalKnowledge target) throws ModelNotFoundException {

		boolean doesHeKnow = true;
		GdlTerm toKnow = target.getTerm();

		Matrix m = indistinguishabilityManager.getCurrentMatrix(target.getRole());
		List<Integer> usefulModelsIDs = m.getAllUsefulModelsIds();

		if (usefulModelsIDs.size() > 0) {
			// At least one model ~ from the actual play of the game
			List<Model> usefulModels = sampler.getModelsById(usefulModelsIDs);
			for (Model model : usefulModels) {
				doesHeKnow &= stateMachine.proveRelation((GdlRelation) toKnow.toSentence(), model.getModel());
			}
		} else // use the actual state of the game
			return stateMachine.proveRelation((GdlRelation) toKnow.toSentence(), currentState);

		return doesHeKnow;
	}

	/**
	 * Applies the semantics of the common knowledge target.
	 * Using the current matrices.
	 *
	 * @throws ModelNotFoundException
	 */
	private boolean verifyKnowledge(CommonKnowledge target) throws ModelNotFoundException {

		boolean doesHeKnow = true;
		GdlTerm toKnow = target.getTerm();

		List<Integer> usefulModelsIDs = indistinguishabilityManager.getAllTransitiveClousureUsefulModels();
		System.out.println("Useful models for common knowledge: " + usefulModelsIDs);

		if (usefulModelsIDs.size() > 0) {
			// At least one model ~ from the actual play of the game
			List<Model> usefulModels = sampler.getModelsById(usefulModelsIDs);
			for (Model model : usefulModels) {
				doesHeKnow &= stateMachine.proveRelation((GdlRelation) toKnow.toSentence(), model.getModel());
			}
		} else // use the actual state of the game
			return stateMachine.proveRelation((GdlRelation) toKnow.toSentence(), currentState);

		return doesHeKnow;

	}
}