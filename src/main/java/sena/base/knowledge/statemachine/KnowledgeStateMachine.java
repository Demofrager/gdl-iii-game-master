package sena.base.knowledge.statemachine;

import java.util.List;
import java.util.Set;

import javax.naming.SizeLimitExceededException;

import org.ggp.base.util.gdl.grammar.GdlSentence;
import org.ggp.base.util.statemachine.Move;
import org.ggp.base.util.statemachine.exceptions.MoveDefinitionException;
import org.ggp.base.util.statemachine.exceptions.TransitionDefinitionException;

import sena.base.knowledge.KnowledgeRule;
import sena.base.knowledge.bag.exception.ModelNotFoundException;
import sena.base.util.statemachine.Percept;
import sena.base.util.statemachine.exceptions.PerceptDefinitionException;

public abstract class KnowledgeStateMachine {

	/**
	 * This method initialises target rules to work for the knowledge.
	 * It is necessary because we can only know the necessary knowledge terms
	 * after the first round (usually when random assigns the unknown factor)
	 * @param targetRules
	 */
	public abstract void initialize ( Set<KnowledgeRule> targetRules);

	/** On this method it is necessary to update the implementation
	 *  used in the semantics used (i.e. DEL)
	 *
	 * @param stateMachine - to randomise next states
	 * @param roles - roles of the players
	 * @param previousMoves - their submitted moves
	 * @param percepts - the receiving precepts according to the last previousMoves
	 * @throws MoveDefinitionException
	 * @throws PerceptDefinitionException
	 * @throws TransitionDefinitionException
	 * @throws SizeLimitExceededException
	 */
	public abstract void updateKnowledgeStage( List<Move> previousMoves,
			List<List<Percept>> percepts) throws MoveDefinitionException, PerceptDefinitionException, TransitionDefinitionException, SizeLimitExceededException;

	/**On this method one needs to implement the semantics of the logic
	 * that is using to calculate the knows literals. Then it's just
	 * necessary to return them to the state machine, and she will do the rest.
	 *
	 * This method should be called at least once per turn, but perhaps is
	 * better to implement a cache in the stateMachine to avoid unnecessary calls.
	 *
	 * @return Set of knowledge gdl sentences that are true at the current turn.
	 * @throws ModelNotFoundException
	 * @throws TransitionDefinitionException
	 */
	public abstract Set<GdlSentence> getKnowledgeStage() throws ModelNotFoundException, TransitionDefinitionException;

}
