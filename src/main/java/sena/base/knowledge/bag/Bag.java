package sena.base.knowledge.bag;

import java.util.ArrayList;
import java.util.List;

import org.ggp.base.util.statemachine.MachineState;
import org.ggp.base.util.statemachine.Move;

import sena.base.knowledge.bag.exception.ModelNotFoundException;
import sena.base.knowledge.model.Model;

/**
 * Where the models will be stored
 *
 * @author demofrager
 *
 */
public class Bag {

	private List<Model> models;
	private int size;


	public Bag( int size, MachineState initialState) {
		this.size = size;
		models = new ArrayList<Model>(this.size);

		Model initialModel ;
		for (int i = 0; i < this.size; i++) {
			initialModel = new Model(initialState.clone());
			models.add(initialModel);
		}
	}

	/** Only for JUnit tests to check if we with few models don't have duplicates
	 * @return
	 */
	public boolean isConsistent() {
		boolean consistent = true;
		Model model1, model2;

		for (int i = 0; i < models.size(); i++) {
			model1 = models.get(i);
			for (int j = i; j < models.size(); j++) {
				if (i != j) {
					model2 = models.get(j);
					consistent &= !model1.equals(model2);
				}
			}
		}

		return consistent;
	}

	public boolean exists(List<List<Move>> path) {

		for(Model model : models)
			if(model.getModelID().equals(path)) return true;

		return false;

	}

	public int size(){
		return size;
	}

	public Model getModel(int modelID) throws ModelNotFoundException{
		if(modelID >= models.size())
			throw new ModelNotFoundException(modelID);

		return models.get(modelID);
	}

	public List<Model> getAll(){
		return models;
	}

	public void replaceModel(int modelID, Model newModel ) throws ModelNotFoundException{
		Model m = getModel(modelID);
		m.setModel(newModel.getModel());
		m.setModelID(newModel.getModelID());
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("Bag [\n");
		for(int i = 0; i < models.size(); i++ )
			sb.append(i + ": " + models.get(i).toString());
		sb.append( "]");

		return  sb.toString();
	}

}
