package sena.base.knowledge.sampler;

import java.util.ArrayList;
import java.util.List;

import sena.base.knowledge.bag.Bag;
import sena.base.knowledge.bag.exception.ModelNotFoundException;
import sena.base.knowledge.model.Model;

/**
 * This class represents a sampler that can be used for model sampling.
 *
 * @author demofrager
 *
 */
public abstract class Sampler {


	private Bag myBag;

	/** Gets a list of models in the bag, given their id.
	 *
	 * @param ids
	 * @return
	 * @throws ModelNotFoundException
	 */
	public List<Model> getModelsById(List<Integer> ids) throws ModelNotFoundException{
		List<Model> toReturn = new ArrayList<Model>(ids.size());

		for (int i : ids) {
			toReturn.add(myBag.getModel(i));
		}
		return toReturn;
	}

	/**Returns the sampler's bag.
	 *
	 * @return
	 */
	public Bag getBag(){
		return myBag;
	}
}
