package sena.base.knowledge.sampler.exceptions;

public class SizeOfResampleException extends Exception {

	/**
	 *
	 */
	private static final long serialVersionUID = 1L;

	private int resamples_size, matrices_size;

	public  SizeOfResampleException (int resamples_size, int matrices_size) {
		this.resamples_size = resamples_size;
		this.matrices_size = matrices_size;
	}

	@Override
	public String toString() {
		return resamples_size + " missmatch " + matrices_size;
	}
}
