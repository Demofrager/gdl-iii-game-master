package sena.base.knowledge.sampler;

import java.util.List;
import java.util.Map;
import java.util.Set;

import org.ggp.base.util.statemachine.Move;
import org.ggp.base.util.statemachine.Role;
import org.ggp.base.util.statemachine.exceptions.MoveDefinitionException;
import org.ggp.base.util.statemachine.exceptions.TransitionDefinitionException;

import sena.base.knowledge.KnowledgeRule;
import sena.base.knowledge.bag.exception.InconsistentPathToCurrentLvlException;
import sena.base.knowledge.bag.exception.ModelNotFoundException;
import sena.base.knowledge.indistinguishability.Matrix;
import sena.base.knowledge.model.Model;
import sena.base.util.statemachine.exceptions.PerceptDefinitionException;
/** This class represents a dynamic epistemic sampler.
 *
 * 	Since we are using DEL it is necessary to methods to update the epistemic state of the models.
 * 	We need a re-sample and a next level
 *
 * @author demofrager
 *
 */
public abstract class DynamicEpistemicSampler extends Sampler {

	// Knowns relations identified that we need to test if are true or not
	protected Set<KnowledgeRule> targetKnowsRules;

	public abstract List<Model> resample(List<Integer> modelIDs, int level, List<Move> leveledJointMoves)
			throws ModelNotFoundException, InconsistentPathToCurrentLvlException,
			MoveDefinitionException, TransitionDefinitionException, PerceptDefinitionException;

	public abstract List<Model> sampleNextLvl(List<Move> recentMoves, List<Integer> inconsistentModelsIDs)
			throws ModelNotFoundException, MoveDefinitionException,
			TransitionDefinitionException, InconsistentPathToCurrentLvlException, PerceptDefinitionException;

	public abstract void updateModelsEpistemicState(Map<Role, Matrix> indMatrices) throws ModelNotFoundException;

	public abstract void updateModelsEpistemicState(Map<Role, Matrix> snapshot, List<Integer> inconsistentModelsIDs)
			throws ModelNotFoundException;


	public abstract int getSamplerLvl();

	public void initialize(Set<KnowledgeRule> targetKnowsRules) {
		this.targetKnowsRules = targetKnowsRules;
	}


}
