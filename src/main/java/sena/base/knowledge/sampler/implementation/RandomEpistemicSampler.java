package sena.base.knowledge.sampler.implementation;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.ggp.base.util.gdl.grammar.GdlPool;
import org.ggp.base.util.gdl.grammar.GdlRelation;
import org.ggp.base.util.gdl.grammar.GdlSentence;
import org.ggp.base.util.gdl.grammar.GdlTerm;
import org.ggp.base.util.statemachine.MachineState;
import org.ggp.base.util.statemachine.Move;
import org.ggp.base.util.statemachine.Role;
import org.ggp.base.util.statemachine.exceptions.MoveDefinitionException;
import org.ggp.base.util.statemachine.exceptions.TransitionDefinitionException;

import sena.base.knowledge.CommonKnowledge;
import sena.base.knowledge.KnowledgeRule;
import sena.base.knowledge.PersonalKnowledge;
import sena.base.knowledge.bag.Bag;
import sena.base.knowledge.bag.exception.InconsistentPathToCurrentLvlException;
import sena.base.knowledge.bag.exception.ModelNotFoundException;
import sena.base.knowledge.indistinguishability.Matrix;
import sena.base.knowledge.model.Model;
import sena.base.knowledge.sampler.DynamicEpistemicSampler;
import sena.base.knowledge.sampler.exceptions.SizeOfResampleException;
import sena.base.util.path.PathUtilities;
import sena.base.util.statemachine.III_StateMachine;
import sena.base.util.statemachine.Percept;
import sena.base.util.statemachine.exceptions.PerceptDefinitionException;

public class RandomEpistemicSampler extends DynamicEpistemicSampler {

	private int currentLevel;
	private III_StateMachine stateMachine;
	private MachineState intialState;

	private Bag myBag;
	private List<Role> roles;


	public RandomEpistemicSampler(III_StateMachine stateMachine, List<Role> roles, int numberOfModels,
			MachineState initialState) {

		this.roles = roles;
		this.stateMachine = stateMachine;
		this.intialState = initialState.clone();
		currentLevel = 0;
		myBag = new Bag(numberOfModels, this.intialState);
	}


	/**
	 * This method samples a next level on our samples. It takes the
	 * indistinguishability relations of the last turn, so we can update the
	 * states correctly to what is true knowledge in the models
	 *
	 * @param indMatrices
	 * @return
	 * @throws ModelNotFoundException
	 * @throws MoveDefinitionException
	 * @throws TransitionDefinitionException
	 * @throws InconsistentPathToCurrentLvlException
	 * @throws PerceptDefinitionException
	 */
	@Override
	public List<Model> sampleNextLvl(List<Move> recentMoves, List<Integer> inconsistentModelsIDs) throws ModelNotFoundException, MoveDefinitionException,
			TransitionDefinitionException, InconsistentPathToCurrentLvlException, PerceptDefinitionException {

		List<Model> aux = new ArrayList<Model>(myBag.size());
		for (int i = 0; i < myBag.size(); i++) {
			//if(!inconsistentModelsIDs.contains(i)) // Avoid unnecessary exploration. Probably troublesome
				aux.add(sampleNextLvl(i));
		}
		currentLevel++;

		List<Model> toReturn = new ArrayList<Model>(aux); // Don't want to pass
															// pointers to the
															// models inside the
															// list. Just a hard
															// copy.

		return toReturn;
	}

	private Model sampleNextLvl(int modelID) throws ModelNotFoundException, MoveDefinitionException,
			TransitionDefinitionException, PerceptDefinitionException {

		Model toUpdate = myBag.getModel(modelID);
		MachineState stateToUpdate = toUpdate.getModel().clone();
		List<List<Move>> pathToUpdate = new ArrayList<List<Move>>(toUpdate.getModelID());

		//System.err.println("NextLVL: " + modelID);

		//System.out.println(stateToUpdate);
		//System.out.println(stateMachine.getLegalJointMoves(stateToUpdate));
		List<Move> jointMove = stateMachine.getRandomJointMove(stateToUpdate);

		List<List<Percept>> jointPercepts = stateMachine.getPercepts(stateToUpdate, jointMove);
		List<List<List<Percept>>> modelPerceptPathtoUpdate = PathUtilities
				.perceptsPathBuilder(toUpdate.getModelPerceptHistory(), jointPercepts);

		stateToUpdate = nextStateWithKnowledgeInertia(stateToUpdate, jointMove);

		pathToUpdate.add(jointMove);

		return updateModel(toUpdate, stateToUpdate, pathToUpdate, modelPerceptPathtoUpdate);
	}

	@Override
	public List<Model> resample(List<Integer> modelIDs, int level, List<Move> leveledJointMoves)
			throws ModelNotFoundException, InconsistentPathToCurrentLvlException, MoveDefinitionException,
			TransitionDefinitionException, PerceptDefinitionException {
		List<Model> newModels = new ArrayList<Model>(myBag.size());

		for (int i : modelIDs) {
			newModels.add(resample(i, level));
		}

		return newModels;
	}

	private Model resample(int modelID, int level) throws ModelNotFoundException, InconsistentPathToCurrentLvlException,
			MoveDefinitionException, TransitionDefinitionException, PerceptDefinitionException {

		Model toResample = myBag.getModel(modelID);
		MachineState state;

		if (level == 0)
			state = intialState.clone();
		else
			state = toResample.getModel().clone();

		List<List<Move>> newStatePath = new ArrayList<List<Move>>(level + 1);
		List<List<List<Percept>>> modelPerceptPath = new ArrayList<List<List<Percept>>>(level + 1);

		if (level > 0) {
			for (int i = 0; i < level; i++) {
				newStatePath.add(toResample.getModelID().get(i));
				modelPerceptPath.add(toResample.getModelPerceptHistory().get(i));
			}
		}

		List<Move> jointMove = stateMachine.getRandomJointMove(state);

		newStatePath.add(jointMove);
		modelPerceptPath.add(stateMachine.getPercepts(state, jointMove));
		state = nextStateWithKnowledgeInertia(state, jointMove);

		return updateModel(toResample, state, newStatePath, modelPerceptPath);
	}

	// AUXILIAR METHODS
	private MachineState nextStateWithKnowledgeInertia(MachineState state, List<Move> jointMove)
			throws TransitionDefinitionException {

		Set<GdlSentence> contents = new HashSet<>(state.getContents());

		MachineState aux = stateMachine.getNextState(state, jointMove);

		Set<GdlSentence> newContents = new HashSet<>();
		for (GdlSentence sentence : contents) {
			if (sentence.getName().equals(GdlPool.KNOWS)) {
				newContents.add(sentence);
			}
		}

		newContents.addAll(aux.getContents());
		aux = new MachineState(newContents);

		return aux;

	}

	@Override
	public void updateModelsEpistemicState(Map<Role, Matrix> indMatrices) throws ModelNotFoundException {

		for (int i = 0; i < myBag.size(); i++) {
			Model model = myBag.getModel(i);
			MachineState newEpistemicModel = sampleKnowledge(i, model.getModel(), indMatrices);

			updateModel(model, newEpistemicModel, model.getModelID(), model.getModelPerceptHistory());
		}
	}

	public void updateModelsEpistemicState(List<Model> resamples, Map<Role, Matrix> indMatrices)
			throws ModelNotFoundException, SizeOfResampleException {
		if (resamples.size() != indMatrices.size())
			throw new SizeOfResampleException(resamples.size(), indMatrices.size());

		for (int i = 0; i < resamples.size(); i++) {
			Model model = resamples.get(i);
			MachineState newEpistemicModel = sampleKnowledge(i, model.getModel(), indMatrices);

			updateModel(model, newEpistemicModel, model.getModelID(), model.getModelPerceptHistory());
		}
	}

	private MachineState sampleKnowledge(int modelID, MachineState state, Map<Role, Matrix> indMatrices)
			throws ModelNotFoundException {
		Set<GdlSentence> knowledgeStage = new HashSet<GdlSentence>();

		for (KnowledgeRule knows : targetKnowsRules) {
			if (knows instanceof PersonalKnowledge) {
				PersonalKnowledge target = (PersonalKnowledge) knows;
				if (sampleKnowledge(modelID, target, state, indMatrices))
					knowledgeStage.add(target.getSentence());
			} else {
				CommonKnowledge target = (CommonKnowledge) knows;
				if (sampleKnowledge(modelID, target, state))
					knowledgeStage.add(target.getSentence());
			}
		}

		knowledgeStage.addAll(state.getContents());
		MachineState newEpistemicState = new MachineState(knowledgeStage);
		return newEpistemicState;
	}

	private boolean sampleKnowledge(int modelID, PersonalKnowledge target, MachineState state,
			Map<Role, Matrix> indMatrices) throws ModelNotFoundException {

		GdlTerm toKnow = target.getTerm();

		boolean result = stateMachine.proveRelation((GdlRelation) toKnow.toSentence(), state);

		Matrix m = indMatrices.get(target.getRole());
		List<Integer> indistinguishableModelsIDs;
		List<Model> indistinguishableModels;

		indistinguishableModelsIDs = m.getAllIndistinguishableModelsIds(modelID);

		if (indistinguishableModelsIDs.size() > 0) {
			indistinguishableModels = getModelsById(indistinguishableModelsIDs);
			for (Model model : indistinguishableModels) {
				result &= stateMachine.proveRelation((GdlRelation) toKnow.toSentence(), model.getModel());
			}
		}

		return result;
	}

	private boolean sampleKnowledge(int modelID, CommonKnowledge target, MachineState state) {
		// TODO Auto-generated method stub
		return false;
	}

	/**
	 * Updates the specific model with the given information.
	 *
	 * @param model
	 * @param newState
	 * @param newPath
	 * @param modelPerceptPath
	 * @return
	 */
	private Model updateModel(Model model, MachineState newState, List<List<Move>> newPath,
			List<List<List<Percept>>> modelPerceptPath) {
		model.setModel(newState);
		model.setModelID(newPath);
		model.setModelPerceptHistory(modelPerceptPath);
		return model;
	}

	/**
	 * Gets a list of models in the bag, given their id.
	 *
	 * @param ids
	 * @return
	 * @throws ModelNotFoundException
	 */
	@Override
	public List<Model> getModelsById(List<Integer> ids) throws ModelNotFoundException {
		List<Model> toReturn = new ArrayList<Model>(ids.size());

		for (int i : ids) {
			toReturn.add(myBag.getModel(i));
		}
		return toReturn;
	}

	/**
	 * Returns the sampler's bag.
	 *
	 * @return
	 */
	@Override
	public Bag getBag() {
		return myBag;
	}

	/**
	 *
	 * @return
	 */
	@Override
	public int getSamplerLvl() {
		return currentLevel;
	}

	@Override
	public void updateModelsEpistemicState(Map<Role, Matrix> currentMatrices, List<Integer> resampleModels) throws ModelNotFoundException {


		for(int i: resampleModels){
			Model model = myBag.getModel(i);
			MachineState newEpistemicModel = sampleKnowledge(i, model.getModel(), currentMatrices);

			updateModel(model, newEpistemicModel, model.getModelID(), model.getModelPerceptHistory());
		}
	}

}
