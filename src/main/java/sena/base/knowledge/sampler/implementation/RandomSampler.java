package sena.base.knowledge.sampler.implementation;

import java.util.ArrayList;
import java.util.List;

import org.ggp.base.util.statemachine.MachineState;
import org.ggp.base.util.statemachine.Move;
import org.ggp.base.util.statemachine.Role;
import org.ggp.base.util.statemachine.exceptions.MoveDefinitionException;
import org.ggp.base.util.statemachine.exceptions.TransitionDefinitionException;

import sena.base.knowledge.bag.Bag;
import sena.base.knowledge.bag.exception.InconsistentPathToCurrentLvlException;
import sena.base.knowledge.bag.exception.ModelNotFoundException;
import sena.base.knowledge.model.Model;
import sena.base.knowledge.sampler.Sampler;
import sena.base.util.path.PathUtilities;
import sena.base.util.statemachine.III_StateMachine;
import sena.base.util.statemachine.Percept;
import sena.base.util.statemachine.exceptions.PerceptDefinitionException;

public class RandomSampler extends Sampler{

	int currentLevel;
	III_StateMachine stateMachine;
	MachineState intialState;

	Bag myBag;
	List<Role> roles;


	public RandomSampler (III_StateMachine stateMachine, List<Role> roles, int numberOfModels, MachineState initialState){
		this.roles = roles;

		this.stateMachine = stateMachine;
		this.intialState = initialState.clone();
		currentLevel = 0;
		myBag = new Bag( numberOfModels, this.intialState);
	}


	public List<Model> sampleNextLvl() throws ModelNotFoundException, MoveDefinitionException,
		TransitionDefinitionException, InconsistentPathToCurrentLvlException, PerceptDefinitionException{

		List<Model> aux = new ArrayList<Model>(myBag.size());
		for (int i = 0; i < myBag.size(); i++){
			aux.add(sampleNextLvl(i));
		}
		currentLevel ++;

		List<Model> toReturn = new ArrayList<Model>(aux); // Don't want to pass pointers to the models inside
													 	  // the list. Just a hard copy.

		System.out.println(myBag);
		return toReturn;
	}

	private Model sampleNextLvl(int modelID) throws ModelNotFoundException, MoveDefinitionException,
		TransitionDefinitionException, PerceptDefinitionException{

		Model toUpdate = myBag.getModel(modelID);
		MachineState stateToUpdate = toUpdate.getModel().clone();
		List<List<Move>> pathToUpdate = new ArrayList<List<Move>> (toUpdate.getModelID());

		List<Move> jointMove = stateMachine.getRandomJointMove(stateToUpdate);

		List<List<Move>> pathToCheck = PathUtilities.movesPathBuilder(pathToUpdate, jointMove);

		if(myBag.exists(pathToCheck)){ 									// If the number of models is big this is not necessary
																		// Check one time if the path already exists
			jointMove = stateMachine.getRandomJointMove(stateToUpdate); // What is the probability to get another
																		// random joint move and is repeated?
		}

		List<List<Percept>> jointPercepts = stateMachine.getPercepts(stateToUpdate, jointMove);
		List<List<List<Percept>>> modelPerceptPathtoUpdate = PathUtilities.perceptsPathBuilder(toUpdate.getModelPerceptHistory(), jointPercepts);

		stateToUpdate = stateMachine.getNextState(stateToUpdate, jointMove);
		pathToUpdate.add(jointMove);

		return updateModel(toUpdate, stateToUpdate, pathToUpdate, modelPerceptPathtoUpdate);
	}

	/**Re-samples the given models by random sequences of
	 * joint actions
	 *
	 * @param modelID
	 * @return
	 * @throws ModelNotFoundException
	 * @throws InconsistentPathToCurrentLvlException
	 * @throws MoveDefinitionException
	 * @throws TransitionDefinitionException
	 * @throws PerceptDefinitionException
	 */
	public List<Model> resample(List<Integer> modelIDs) throws ModelNotFoundException,
	InconsistentPathToCurrentLvlException, MoveDefinitionException, TransitionDefinitionException, PerceptDefinitionException{
		List<Model> newModels = new ArrayList<Model>(modelIDs.size());

		for(int i : modelIDs)
			newModels.add(resample(i));

		return newModels;
	}

	/**Re-samples the given model by random sequences of
	 * joint actions
	 *
	 * @param modelID
	 * @return
	 * @throws ModelNotFoundException
	 * @throws InconsistentPathToCurrentLvlException
	 * @throws MoveDefinitionException
	 * @throws TransitionDefinitionException
	 * @throws PerceptDefinitionException
	 */
	public Model resample(int modelID) throws ModelNotFoundException,
		InconsistentPathToCurrentLvlException, MoveDefinitionException, TransitionDefinitionException, PerceptDefinitionException{

		Model toResample = myBag.getModel(modelID);


		MachineState newState = intialState.clone();
		List<List<Move>> newStatePath = new ArrayList<List<Move>>(currentLevel);
		List<List<List<Percept>>> modelPerceptPath = new ArrayList<List<List<Percept>>>(currentLevel);
		List<Move> jointMove;

		for(int i = 0; i < currentLevel; i++){
			jointMove = stateMachine.getRandomJointMove(newState);
			modelPerceptPath.add(stateMachine.getPercepts(newState, jointMove));
			newState = stateMachine.getNextState(newState, jointMove);
			newStatePath.add(jointMove);
		}


		return updateModel(toResample, newState, newStatePath, modelPerceptPath);
	}


	/**Updates the specific model with the given information.
	 *
	 * @param model
	 * @param newState
	 * @param newPath
	 * @param modelPerceptPath
	 * @return
	 */
	private Model updateModel(Model model, MachineState newState, List<List<Move>> newPath, List<List<List<Percept>>> modelPerceptPath){
		model.setModel(newState);
		model.setModelID(newPath);
		model.setModelPerceptHistory(modelPerceptPath);
		return model;
	}

	/** Gets a list of models in the bag, given their id.
	 *
	 * @param ids
	 * @return
	 * @throws ModelNotFoundException
	 */
	@Override
	public List<Model> getModelsById(List<Integer> ids) throws ModelNotFoundException{
		List<Model> toReturn = new ArrayList<Model>(ids.size());

		for(int i : ids){
			toReturn.add(myBag.getModel(i));
		}
		return toReturn;
	}

	/**Returns the sampler's bag.
	 *
	 * @return
	 */
	@Override
	public Bag getBag(){
		return myBag;
	}

}
