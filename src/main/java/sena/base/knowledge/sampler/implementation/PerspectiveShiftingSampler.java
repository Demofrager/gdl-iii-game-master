package sena.base.knowledge.sampler.implementation;

import java.util.ArrayList;
import java.util.List;

import org.ggp.base.util.gdl.grammar.GdlPool;
import org.ggp.base.util.statemachine.MachineState;
import org.ggp.base.util.statemachine.Move;
import org.ggp.base.util.statemachine.Role;
import org.ggp.base.util.statemachine.exceptions.MoveDefinitionException;
import org.ggp.base.util.statemachine.exceptions.TransitionDefinitionException;

import sena.base.knowledge.bag.Bag;
import sena.base.knowledge.bag.exception.InconsistentPathToCurrentLvlException;
import sena.base.knowledge.bag.exception.ModelNotFoundException;
import sena.base.knowledge.model.Model;
import sena.base.knowledge.sampler.Sampler;
import sena.base.util.path.PathUtilities;
import sena.base.util.statemachine.III_StateMachine;
import sena.base.util.statemachine.Percept;
import sena.base.util.statemachine.exceptions.PerceptDefinitionException;

public class PerspectiveShiftingSampler extends Sampler {

	int currentLevel;
	III_StateMachine stateMachine;
	MachineState intialState;

	Bag myBag;
	List<Role> roles;
	List<Integer> rolesIDsNoRandom;

	// This sampler is not epistemic, but nothing says it can't be changed for the models to be updated
	public PerspectiveShiftingSampler (III_StateMachine stateMachine, List<Role> roles, int numberOfModels, MachineState initialState){
		this.roles = roles;

		rolesIDsNoRandom = new ArrayList<Integer>(roles.size()-1);
		for(int i=0; i < roles.size(); i++){
			if(!roles.get(i).getName().equals(GdlPool.RANDOM)){
				rolesIDsNoRandom.add(i);
			}
		}

		this.stateMachine = stateMachine;
		this.intialState = initialState.clone();
		currentLevel = 0;
		myBag = new Bag( numberOfModels, this.intialState);
	}

	/**
	 * Sampling next level with perspective view. Receives the most recent
	 * moves. It works as for each modelID % roles.size. It always makes sure
	 *
	 * @param recentMoves
	 * @return
	 * @throws ModelNotFoundException
	 * @throws MoveDefinitionException
	 * @throws TransitionDefinitionException
	 * @throws InconsistentPathToCurrentLvlException
	 * @throws PerceptDefinitionException
	 */
	public List<Model> sampleNextLvl(List<Move> recentMoves) throws ModelNotFoundException, MoveDefinitionException,
			TransitionDefinitionException, InconsistentPathToCurrentLvlException, PerceptDefinitionException {

		List<Model> aux = new ArrayList<Model>(myBag.size());

		for (int i = 0; i < myBag.size(); i++) {
			aux.add(sampleNextLvl(i, rolesIDsNoRandom.get(i % rolesIDsNoRandom.size()), recentMoves));
		}
		currentLevel++;

		List<Model> toReturn = new ArrayList<Model>(aux); // Don't want to pass
															// pointers to the
															// models inside
															// the list. Just a
															// hard copy.

		System.out.println(myBag);
		return toReturn;
	}

	private Model sampleNextLvl(int modelID, int roleID, List<Move> recentMoves) throws ModelNotFoundException,
			MoveDefinitionException, TransitionDefinitionException, PerceptDefinitionException {

		Model toUpdate = myBag.getModel(modelID);
		MachineState stateToUpdate = toUpdate.getModel().clone();
		List<List<Move>> pathToUpdate = new ArrayList<List<Move>>(toUpdate.getModelID());
		Role role = roles.get(roleID);
		Move move = recentMoves.get(roleID);
		List<Move> jointMove ;

		if(stateMachine.getLegalMoves(stateToUpdate, role).contains(move))// Consistency check
			jointMove = stateMachine.getRandomJointMove(stateToUpdate, role, move);
		else{
			jointMove = stateMachine.getRandomJointMove(stateToUpdate);
		}


		List<List<Move>> pathToCheck = PathUtilities.movesPathBuilder(pathToUpdate, jointMove);

		if (myBag.exists(pathToCheck)) { // If the number of models is big this might not be necessary.
										 // Checks one time if the path already exists
			jointMove = stateMachine.getRandomJointMove(stateToUpdate, role, move); // What is the probability to
			   														// get another move and is repeated?

		}

		List<List<Percept>> jointPercepts = stateMachine.getPercepts(stateToUpdate, jointMove);
		List<List<List<Percept>>> modelPerceptPathtoUpdate = PathUtilities
				.perceptsPathBuilder(toUpdate.getModelPerceptHistory(), jointPercepts);

		stateToUpdate = stateMachine.getNextState(stateToUpdate, jointMove);
		pathToUpdate.add(jointMove);

		return updateModel(toUpdate, stateToUpdate, pathToUpdate, modelPerceptPathtoUpdate);
	}

	/**
	 * Re-sample the models given by the modelsID list, to a perspective of the
	 * given role. The path to copy is the actual role history of the game.
	 *
	 * @param modelIDs
	 * @param role
	 * @param pathToCopy
	 * @return
	 * @throws ModelNotFoundException
	 * @throws InconsistentPathToCurrentLvlException
	 * @throws MoveDefinitionException
	 * @throws TransitionDefinitionException
	 * @throws PerceptDefinitionException
	 */
	public List<Model> resample(List<Integer> modelIDs, List<Role> roles, List<List<Move>> pathToCopy)
			throws ModelNotFoundException, InconsistentPathToCurrentLvlException, MoveDefinitionException,
			TransitionDefinitionException, PerceptDefinitionException {

		List<Model> newModels = new ArrayList<Model>(modelIDs.size());

		for (int i : modelIDs)
			newModels.add(resample(i, rolesIDsNoRandom.get(i % this.rolesIDsNoRandom.size()), pathToCopy));

		return newModels;
	}

	/**
	 * Re-sample the model given by the modelsID, to a perspective of the given
	 * role. The path to copy is the actual role history of the game.
	 *
	 * @param modelID
	 * @param role
	 * @param pathToCopy
	 * @return - the new re-sampled model
	 * @throws ModelNotFoundException
	 * @throws InconsistentPathToCurrentLvlException
	 * @throws MoveDefinitionException
	 * @throws TransitionDefinitionException
	 * @throws PerceptDefinitionException
	 */
	public Model resample(int modelID, int roleID, List<List<Move>> pathToCopy)
			throws ModelNotFoundException, InconsistentPathToCurrentLvlException, MoveDefinitionException,
			PerceptDefinitionException, TransitionDefinitionException {

		if (pathToCopy.size() != currentLevel)
			throw new InconsistentPathToCurrentLvlException(pathToCopy);

		Model toResample = myBag.getModel(modelID);

		MachineState newState = intialState.clone();
		List<List<Move>> newStatePath = new ArrayList<List<Move>>(currentLevel);
		List<List<List<Percept>>> modelPerceptPath = new ArrayList<List<List<Percept>>>(currentLevel);
		List<Move> jointMove;
		Move move;
		Role role = roles.get(roleID);

		for (int i = 0; i < currentLevel - 1; i++) {
			move = pathToCopy.get(i).get(roleID);

			if(stateMachine.getLegalMoves(newState, role).contains(move))// Consistency check
				jointMove = stateMachine.getRandomJointMove(newState, role, move);
			else{
				jointMove = stateMachine.getRandomJointMove(newState);
			}

			modelPerceptPath.add(stateMachine.getPercepts(newState, jointMove));
			newState = stateMachine.getNextState(newState, jointMove);
			newStatePath.add(jointMove);
		}

		jointMove = stateMachine.getRandomJointMove(newState);
		modelPerceptPath.add(stateMachine.getPercepts(newState, jointMove));

		newState = stateMachine.getNextState(newState, jointMove);
		newStatePath.add(jointMove);

		return updateModel(toResample, newState, newStatePath, modelPerceptPath);
	}

	/**Updates the specific model with the given information.
	 *
	 * @param model
	 * @param newState
	 * @param newPath
	 * @param modelPerceptPath
	 * @return
	 */
	private Model updateModel(Model model, MachineState newState, List<List<Move>> newPath, List<List<List<Percept>>> modelPerceptPath){
		model.setModel(newState);
		model.setModelID(newPath);
		model.setModelPerceptHistory(modelPerceptPath);
		return model;
	}

	/** Gets a list of models in the bag, given their id.
	 *
	 * @param ids
	 * @return
	 * @throws ModelNotFoundException
	 */
	@Override
	public List<Model> getModelsById(List<Integer> ids) throws ModelNotFoundException{
		List<Model> toReturn = new ArrayList<Model>(ids.size());

		for(int i : ids){
			toReturn.add(myBag.getModel(i));
		}
		return toReturn;
	}

	/**Returns the sampler's bag.
	 *
	 * @return
	 */
	@Override
	public Bag getBag(){
		return myBag;
	}
}
