package sena.base.knowledge.sampler.implementation;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Random;
import java.util.Set;

import org.ggp.base.util.gdl.grammar.GdlPool;
import org.ggp.base.util.gdl.grammar.GdlRelation;
import org.ggp.base.util.gdl.grammar.GdlSentence;
import org.ggp.base.util.gdl.grammar.GdlTerm;
import org.ggp.base.util.statemachine.MachineState;
import org.ggp.base.util.statemachine.Move;
import org.ggp.base.util.statemachine.Role;
import org.ggp.base.util.statemachine.exceptions.MoveDefinitionException;
import org.ggp.base.util.statemachine.exceptions.TransitionDefinitionException;

import sena.base.knowledge.CommonKnowledge;
import sena.base.knowledge.KnowledgeRule;
import sena.base.knowledge.PersonalKnowledge;
import sena.base.knowledge.bag.Bag;
import sena.base.knowledge.bag.exception.InconsistentPathToCurrentLvlException;
import sena.base.knowledge.bag.exception.ModelNotFoundException;
import sena.base.knowledge.model.Model;
import sena.base.knowledge.sampler.Sampler;
import sena.base.util.path.PathUtilities;
import sena.base.util.statemachine.III_StateMachine;
import sena.base.util.statemachine.Percept;
import sena.base.util.statemachine.exceptions.PerceptDefinitionException;

public class PerspectiveShiftingEpistemicModelSampler extends Sampler{

	// For knowledge terms calculation
	private Set<KnowledgeRule> targetKnowsRules;
	private int currentLevel;
	private III_StateMachine stateMachine;
	private MachineState intialState;
	Random myRandom;
	private Bag myBag;
	private List<Role> roles;
	private List<Integer> rolesIDsNoRandom;
	private int randomPlayerID;

	public PerspectiveShiftingEpistemicModelSampler(III_StateMachine stateMachine, List<Role> roles, int numberOfModels, MachineState initialState){
		this.roles = roles;

		rolesIDsNoRandom = new ArrayList<Integer>(roles.size()-1);
		for(int i=0; i < roles.size(); i++){

			if(!roles.get(i).getName().equals(GdlPool.RANDOM)){
				rolesIDsNoRandom.add(i);
			}else randomPlayerID = i;
		}
		myRandom = new Random();
		this.stateMachine = stateMachine;
		this.intialState = initialState.clone();
		currentLevel = 0;
		myBag = new Bag( numberOfModels, this.intialState);
	}

	public void initialize (Set<KnowledgeRule>targetKnowsRules){
		this.targetKnowsRules = targetKnowsRules;
	}

	public List<Model> sampleNextLvl(List<Move> recentMoves) throws ModelNotFoundException, MoveDefinitionException,
			TransitionDefinitionException, InconsistentPathToCurrentLvlException, PerceptDefinitionException {

		List<Model> aux = new ArrayList<Model>(myBag.size());

		for (int i = 0; i < myBag.size(); i++) {
			aux.add(sampleNextLvl(i, rolesIDsNoRandom.get(i % rolesIDsNoRandom.size()), recentMoves));
		}
		currentLevel++;

		List<Model> toReturn = new ArrayList<Model>(aux);

		System.out.println(myBag);
		return toReturn;
	}

	private Model sampleNextLvl(int modelID, int roleID, List<Move> recentMoves)
			throws ModelNotFoundException, MoveDefinitionException, PerceptDefinitionException, TransitionDefinitionException {

		Model toUpdate = myBag.getModel(modelID);
		MachineState stateToUpdate = toUpdate.getModel().clone();
		List<List<Move>> pathToUpdate = new ArrayList<List<Move>>(toUpdate.getModelID());

		List<Move> jointMove = stateMachine.getRandomJointMove(stateToUpdate, roles.get(roleID),
				recentMoves.get(roleID));

		List<List<Move>> pathToCheck = PathUtilities.movesPathBuilder(pathToUpdate, jointMove);

		List<List<Percept>> jointPercepts = stateMachine.getPercepts(stateToUpdate, jointMove);
		List<List<List<Percept>>> modelPerceptPathtoUpdate = PathUtilities
				.perceptsPathBuilder(toUpdate.getModelPerceptHistory(), jointPercepts);


		MachineState contentsOfState = nextStateWithKnowledgeInertia(stateToUpdate, jointMove);

		pathToUpdate.add(jointMove);

		MachineState epistemicState = sampleKnowledge(contentsOfState);

		return updateModel(toUpdate, epistemicState, pathToCheck, modelPerceptPathtoUpdate);
	}

	/**
	 * Re-sample the models given by the modelsID list, to a perspective of the
	 * given role. The path to copy is the actual role history of the game.
	 *
	 * @param modelIDs
	 * @param role
	 * @param pathToCopy
	 * @return
	 * @throws ModelNotFoundException
	 * @throws InconsistentPathToCurrentLvlException
	 * @throws MoveDefinitionException
	 * @throws TransitionDefinitionException
	 * @throws PerceptDefinitionException
	 */
	public List<Model> resample(List<Integer> modelIDs, List<Role> roles, List<List<Move>> pathToCopy)
			throws ModelNotFoundException, InconsistentPathToCurrentLvlException, MoveDefinitionException,
			TransitionDefinitionException, PerceptDefinitionException {

		List<Model> newModels = new ArrayList<Model>(modelIDs.size());

		for (int i : modelIDs)
			newModels.add(resample(i, rolesIDsNoRandom.get(i % this.rolesIDsNoRandom.size()), pathToCopy));

		return newModels;
	}

	/**
	 * Re-sample the model given by the modelsID, to a perspective of the given
	 * role. The path to copy is the actual role history of the game.
	 *
	 * @param modelID
	 * @param role
	 * @param pathToCopy
	 * @return - the new resampeled model
	 * @throws ModelNotFoundException
	 * @throws InconsistentPathToCurrentLvlException
	 * @throws MoveDefinitionException
	 * @throws TransitionDefinitionException
	 * @throws PerceptDefinitionException
	 */
	public Model resample(int modelID, int roleID, List<List<Move>> pathToCopy)
			throws ModelNotFoundException, InconsistentPathToCurrentLvlException, MoveDefinitionException,
			PerceptDefinitionException, TransitionDefinitionException {

		if (pathToCopy.size() != currentLevel)
			throw new InconsistentPathToCurrentLvlException(pathToCopy);

		Model toResample = myBag.getModel(modelID);

		MachineState newState = intialState.clone();
		List<List<Move>> newStatePath = new ArrayList<List<Move>>(currentLevel);
		List<List<List<Percept>>> modelPerceptPath = new ArrayList<List<List<Percept>>>(currentLevel);
		List<Move> jointMove;
		Move move;
		Role role = roles.get(roleID);

		for (int i = 0; i < currentLevel - 1; i++) {
			move = pathToCopy.get(i).get(roleID);
			if(!stateMachine.getLegalMoves(newState, role).contains(move))
				return resample(modelID, roleID, pathToCopy);

			jointMove = stateMachine.getRandomJointMove(newState, role, move);
			modelPerceptPath.add(stateMachine.getPercepts(newState, jointMove));
			newState = nextStateWithKnowledgeInertia(newState, jointMove);
			newStatePath.add(jointMove);
			newState =  sampleKnowledge(newState);
		}

		jointMove = stateMachine.getRandomJointMove(newState);
		modelPerceptPath.add(stateMachine.getPercepts(newState, jointMove));

		newState = nextStateWithKnowledgeInertia(newState, jointMove);
		newState =  sampleKnowledge(newState);
		newStatePath.add(jointMove);

		return updateModel(toResample, newState, newStatePath, modelPerceptPath);
	}

	// AUXILIAR METHODS
	public MachineState nextStateWithKnowledgeInertia(MachineState state, List<Move> jointMove) throws TransitionDefinitionException {

		Set<GdlSentence> contents = new HashSet<>(state.getContents());

		MachineState aux = stateMachine.getNextState(state, jointMove);

		Set<GdlSentence> newContents = new HashSet<>();
		for(GdlSentence sentence: contents){
			if(sentence.getName().equals(GdlPool.KNOWS)){
				newContents.add(sentence);
			}
		}

		newContents.addAll(aux.getContents());
		aux = new MachineState(newContents);

		return aux;

	}

	private MachineState sampleKnowledge(MachineState state) throws ModelNotFoundException{
		Set<GdlSentence> knowledgeStage = new HashSet<GdlSentence>();

		for(KnowledgeRule knows: targetKnowsRules){
			if(knows instanceof PersonalKnowledge){
				PersonalKnowledge target = (PersonalKnowledge)knows;
				if(myRandom.nextBoolean() && sampleKnowledge(target, state))
					knowledgeStage.add(target.getSentence());
			}
			else{
				CommonKnowledge target = (CommonKnowledge) knows;
				if(myRandom.nextBoolean() && sampleKnowledge(target, state))
					knowledgeStage.add(target.getSentence());
			}
		}

		knowledgeStage.addAll(state.getContents());
		MachineState newEpistemicState = new MachineState(knowledgeStage);
		return newEpistemicState;
	}

	private boolean sampleKnowledge(PersonalKnowledge target, MachineState state) {

		GdlTerm toKnow = target.getTerm();
		return stateMachine.proveRelation((GdlRelation)toKnow.toSentence(), state);
	}

	private boolean sampleKnowledge(CommonKnowledge target, MachineState state) {
		// TODO Auto-generated method stub
		return false;
	}


	@Override
	public List<Model> getModelsById(List<Integer> ids) throws ModelNotFoundException {
		List<Model> toReturn = new ArrayList<Model>(ids.size());

		for(int i : ids){
			toReturn.add(myBag.getModel(i));
		}
		return toReturn;
	}

	@Override
	public Bag getBag() {
		return myBag;
	}

	/**Updates the specific model with the given information.
	 *
	 * @param model
	 * @param newState
	 * @param newPath
	 * @param modelPerceptPath
	 * @return
	 */
	private Model updateModel(Model model, MachineState newState, List<List<Move>> newPath, List<List<List<Percept>>> modelPerceptPath){
		model.setModel(newState);
		model.setModelID(newPath);
		model.setModelPerceptHistory(modelPerceptPath);
		return model;
	}
}
