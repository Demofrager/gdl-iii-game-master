package org.ggp.base.util.gdl.scrambler;

import org.ggp.base.util.gdl.factory.GdlFactory;
import org.ggp.base.util.gdl.factory.exceptions.GdlFormatException;
import org.ggp.base.util.gdl.grammar.Gdl;
import org.ggp.base.util.symbol.factory.exceptions.SymbolFormatException;

public class NoOpGdlScrambler implements GdlScrambler {
    @Override
    public String scramble(Gdl x) { // TODO remove this
        return x.toString().toUpperCase();
    }
    @Override
    public Gdl unscramble(String x) throws SymbolFormatException, GdlFormatException { // TODO remove this
        return GdlFactory.create(x.toLowerCase());
    }
    @Override
    public boolean scrambles() {
        return false;
    }
}